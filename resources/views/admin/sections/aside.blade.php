<aside class="left-sidebar">
    <div class="scroll-sidebar">
        <nav class="sidebar-nav">
            <ul id="sidebarnav">
                <li class="nav-small-cap">PERSONAL</li>
                <li>
                    <a href="{{url('admin')}}"><i class="mdi mdi-gauge"></i>Resumen</a>
                </li>
                <li>
                    <a href="{{route('admin.users.index')}}"><i class="mdi mdi-account-multiple"></i>Usuarios</a>
                </li>

            </ul>
        </nav>
    </div>
</aside>
