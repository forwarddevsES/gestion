<header class="topbar">
    <nav class="navbar top-navbar navbar-expand-md navbar-light">
      <div class="navbar-header">
        <a class="navbar-brand" href="{{url('/')}}">
          <b>
            <img src="{{asset('assets/images/logo-icon.png')}}" alt="homepage" class="dark-logo" />
            <img src="{{asset('images/logo.png')}}" style="width:60px;" alt="homepage" class="light-logo" />
          </b>
          <span>
            <img src="{{asset('assets/images/logo-text.png')}}" alt="homepage" class="dark-logo" />
            <img src="{{asset('images/logo-text.png')}}" class="light-logo" alt="homepage" />
          </span>
        </a>
      </div>
      <div class="navbar-collapse">
        <ul class="navbar-nav mr-auto mt-md-0">
          <li class="nav-item"> <a class="nav-link nav-toggler hidden-md-up text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="mdi mdi-menu"></i></a> </li>
          <li class="nav-item hidden-sm-down search-box"> <a class="nav-link hidden-sm-down text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="ti-search"></i></a>

            <form class="app-search"  action="{{route('buscar')}}" method="post">
              {{ csrf_field() }}
              {{ method_field('POST')}}
              <input type="text" class="form-control" name="q" placeholder="Numero de Orden"> <a class="srh-btn"><i class="ti-close"></i></a>
            </form>
          </li>

            </ul>
            <ul class="navbar-nav my-lg-0">
                <!-- ============================================================== -->
                <!-- Comment -->
                <!-- ============================================================== -->
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle text-muted text-muted waves-effect waves-dark" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="mdi mdi-message"></i>
                        <div class="notify"> <span class="heartbit"></span> <span class="point"></span> </div>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right mailbox scale-up">
                        <ul>
                            <li>
                                <div class="drop-title">Notificaciones</div>
                            </li>
                            <li>
                                <div class="message-center">
                                    <!-- Message -->
                                    <a href="#">
                                        <div class="btn btn-danger btn-circle"><i class="fa fa-link"></i></div>
                                        <div class="mail-contnet">
                                            <h5>Admin</h5> <span class="mail-desc">Notificaciones deshabilitadas</span> <span class="time">9:30 AM</span> </div>
                                    </a>

                                </div>
                            </li>
                            <li>
                                <a class="nav-link text-center" href="javascript:void(0);"> <strong>Ver todas las notificaciones</strong> <i class="fa fa-angle-right"></i> </a>
                            </li>
                        </ul>
                    </div>
                </li>

                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="{{asset('assets/images/avatar.png')}}" alt="user" class="profile-pic" /></a>
                    <div class="dropdown-menu dropdown-menu-right scale-up">
                        <ul class="dropdown-user">
                            <li>
                                <div class="dw-user-box">
                                    <div class="u-img"><img src="{{asset('assets/images/avatar.png')}}" alt="user"></div>
                                    <div class="u-text">
                                        <h4>{{Auth::user()->first_name}}</h4>
                                        <p class="text-muted">{{Auth::user()->last_name}}</p><a href="{{route('settings')}}" class="btn btn-rounded btn-danger btn-sm">Ver Perfil</a></div>
                                </div>
                            </li>
                            <li role="separator" class="divider"></li>

                            <li><a href="{{route('settings')}}"><i class="ti-settings"></i> Ajustes de la cuenta</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('frm-logout').submit();"><i class="fa fa-power-off"></i> Salir</a>
                              <form id="frm-logout" action="{{ route('logout') }}" method="POST" style="display: none;">
                                  {{ csrf_field() }}
                              </form>
                          </li>
                        </ul>
                    </div>
                </li>

            </ul>
        </div>
    </nav>
</header>
