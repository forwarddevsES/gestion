@extends('admin.layouts.admin')
@section('content')
  @php
    $users = App\User::where('group_id', '2')->get();
  @endphp
  <div class="card">
      <div class="card-body">
        <center><h4 class="card-title">Lista de Usuarios</h4></center>
        <center><h6 class="card-subtitle">Si usted desea crear un nuevo usuario haga <a href="{{route('users.create')}}">click aquí</a>.</h6></center>
          <div class="table-responsive m-t-40">
              <table id="myTable" class="table table-bordered table-striped">
                  <thead>
                    <tr>
                        <th>#</th>
                        <th>Nombre</th>
                        <th>Apellido</th>
                        <th>Email</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach ($users as $user)


                      <tr id="user{{$user->id}}" data-toggle="modal" data-target="#infou{{$user->id}}" data-whatever="@mdo">
                          <td>{{$user->id}}</td>
                          <td>{{$user->first_name}}</td>
                          <td>{{$user->last_name}}</td>
                          <td>{{$user->email}}</td>
                      </tr>
                      <div class="modal fade" id="infou{{$user->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1" style="display: none;" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title" id="exampleModalLabel1">{{$user->first_name}} {{$user->last_name}} (#{{$user->id}})</h4>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                                            </div>
                                            <div class="modal-body">
                                                <p>Nombre y Apellido: <b>{{$user->first_name}} {{$user->last_name}}</b> </p>
                                                <p>Email: {{$user->email}}</p>
                                                <p>Teléfono: {{$user->phone}}</p>
                                            </div>
                                            <div class="modal-footer">
                                              <form id="formdelete{{$user->id}}" action="{{route('users.destroy', ['user' => $user])}}" method="post">
                                                {{ csrf_field() }}
                                                {{ method_field('delete') }}
                                              </form>
                                                <button type="button" class="btn btn-danger" id="sa-eliminar{{$user->id}}">Borrar</button>
                                                <a href="{{route('users.edit', ['user' => $user])}}" class="btn btn-primary">Editar</a>
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Aceptar</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                    @endforeach
                    @if (App\User::where('group_id', '2')->count() == 0)
                      <td></td>
                        <td>No se encontraron resultados</td>

                    @endif
                  </tbody>
              </table>
          </div>
      </div>
  </div>

@endsection

@section('scripts')
  <script src="{{asset('assets/plugins/datatables/jquery.dataTables.min.js')}}"></script>
  <!-- start - This is for export functionality only -->
  <script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
  <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
  <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
  <script>
  $(document).ready(function() {
      $('#myTable').DataTable();
      });
      !function($) {
          "use strict";

          var SweetAlert = function() {};

          //examples
          SweetAlert.prototype.init = function() {
          @foreach ($users as $user)
          $('#sa-eliminar{{$user->id}}').click(function(){
              swal({
                  title: "¿Estás seguro?",
                  text: "Eliminarás el usuario '{{$user->first_name}} {{$user->last_name}}'",
                  type: "warning",
                  showCancelButton: true,
                  confirmButtonColor: "#DD6B55",
                  confirmButtonText: "¡Borralo!",
                  cancelButtonText: "Cancelar",
                  closeOnConfirm: false
              }, function(e){
                if (e) {
                  swal("Eliminado", "El usuario fue eliminado.", "success");

                  $('#formdelete{{$user->id}}').submit();
                  $("#user{{$user->id}}").remove();
                }
                else {
                  swal("Cancelado", "Tu usuario está a salvo.", "success");
                }


              });
          });
          @endforeach





          },
          //init
          $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
      }(window.jQuery),

      //initializing
      function($) {
          "use strict";
          $.SweetAlert.init()
      }(window.jQuery);
  </script>
  <script src="{{asset('assets/plugins/sweetalert/sweetalert.min.js')}}"></script>
  <script src="{{asset('assets/plugins/sweetalert/jquery.sweet-alert.custom.js')}}"></script>

@endsection
