
@extends('admin.layouts.admin')
@section('content')
  <div class="row p-20">
    <div class="col-md-5">
      <center><h2>Negocios</h2></center>
      <div class="table-responsive m-t-40">
          <table id="myTable" class="table table-bordered table-striped">
              <thead>
                <tr>
                    <th>#</th>
                    <th>Nombre</th>
                    <th>Usuarios</th>
                </tr>
              </thead>
              <tbody>
                @foreach (App\Business::all() as $business)
                  <tr id="venta{{$business->id}}" data-toggle="modal" data-target="#infon{{$business->id}}" data-whatever="@mdo">
                      <td>{{$business->id}}</td>
                      <td>{{$business->name}}</td>
                      <td>{{$business->users->count()}}</td>
                  </tr>
                  <div class="modal fade" id="infon{{$business->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1" style="display: none;" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title" id="exampleModalLabel1">{{$business->name}} (#{{$business->id}})</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                                        </div>
                                        <div class="modal-body">
                                          <h3>Información</h3>
                                          <p>Nombre: {{$business->name}}</p>
                                          <p>Dirección: {{$business->address}}, {{$business->city}}({{$business->cp}}), {{$business->province}}</p>
                                          <p>Email: {{$business->email}}</p>
                                          <p>Teléfono: {{$business->phone}}</p>
                                          <hr>
                                          <h3>Plan</h3>
                                          <p>Actual: {{$business->lastActiveSubscription()->plan->name}}</p>
                                          <p>Tiempo restante: {{$business->lastActiveSubscription()->remainingDays()}} dias.</p>
                                          <hr>
                                          <h3>Usuarios</h3>
                                          @foreach ($business->users as $user)
                                            <p>{{$user->first_name}} {{$user->last_name}} - {{$user->email}} </p>
                                          @endforeach

                                        </div>
                                        <div class="modal-footer">
                                          <form id="formdelete{{$business->id}}" action="{{route('sales.destroy', ['id' => $business->id])}}" method="post">
                                            {{ csrf_field() }}
                                            {{ method_field('delete') }}
                                          </form>
                                            <button type="button" class="btn btn-danger" id="sa-eliminar{{$business->id}}">Borrar</button>
                                            <a href="{{route('sales.show', ['id' => $business->id])}}" class="btn btn-primary">Ver</a>
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Aceptar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                @endforeach
                @if (App\Business::count() == 0)
                  <td></td>
                    <td>No se encontraron resultados</td>

                @endif
              </tbody>
          </table>
      </div>
    </div>
    <div class="col-md-7">
      @if (\Session::has('message'))
          <div class="alert alert-warning alert-rounded">
              {!! \Session::get('message') !!}
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
          </div>
      @endif
      <div class="row">

        <div class="col-md-6 col-lg-4 col-xlg-2">
          <div class="card card-inverse card-dark">
            <div class="box text-center">
              <h2 class="font-light text-white">{{App\Entry::count()}} </h2>
              <h6 class="text-white">Orden
                @if(App\Entry::count() > 1)es @endif de trabajo</h6>
                </div>
              </div>
            </div>
            <div class="col-md-6 col-lg-4 col-xlg-2">
              <div class="card card-primary card-inverse">
                <div class="box text-center">
                  <h2 class="font-light text-white">{{App\Product::count()}}</h2>
                  <h6 class="text-white">Productos</h6>
                </div>
              </div>
            </div>
            <div class="col-md-6 col-lg-4 col-xlg-2">
              <div class="card card-success card-inverse">
                <div class="box text-center">
                  <h2 class="font-light text-white">{{App\Client::count()}}</h2>
                  <h6 class="text-white">Clientes</h6>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6 col-lg-4 col-xlg-2">
              <div class="card card-info card-dark">
                <div class="box text-center">
                  <h2 class="font-light text-white">{{App\Sale::count()}}</h2>
                    <h6 class="text-white">Ventas</h6>
                </div>
              </div>
            </div>
            <div class="col-md-6 col-lg-4 col-xlg-2">
              <div class="card card-danger card-inverse">
                <div class="box text-center">
                  <h2 class="font-light text-white">{{App\User::count()}}</h2>
                    <h6 class="text-white">Usuarios</h6>
                </div>
              </div>
            </div>
            <div class="col-md-6 col-lg-4 col-xlg-2">
              <div class="card card-warning card-inverse">
                <div class="box text-center">
                  <h2 class="font-light text-white">{{App\Provider::count()}}</h2>
                    <h6 class="text-white">Proveedores</h6>
                </div>
              </div>
            </div>
            </div>
              <div class="table-responsive m-t-40">
                  <table id="myTable" class="table table-bordered table-striped">
                      <thead>
                        <tr>
                            <th>#</th>
                            <th>Cliente</th>
                            <th>Fecha</th>
                            <th>Total</th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach (App\Sale::all() as $sale)
                          <tr id="venta{{$sale->id}}" data-toggle="modal" data-target="#infon{{$sale->id}}" data-whatever="@mdo">
                              <td>{{$sale->id}}</td>
                              <td>{{$sale->client->name}}</td>
                              <td>{{$sale->created_at}}</td>
                              <td>$ {{$sale->final_price}}</td>
                          </tr>
                          <div class="modal fade" id="infon{{$sale->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1" style="display: none;" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h4 class="modal-title" id="exampleModalLabel1">{{$sale->client->name}} (#{{$sale->client->id}})</h4>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                                                </div>
                                                <div class="modal-body">
                                                  @foreach ($sale->items as $item)
                                                    <p>{{$item->product->name}} x {{$item->amount}} $<b>{{$item->product->price}}</b> </p>
                                                  @endforeach

                                                </div>
                                                <div class="modal-footer">
                                                  <form id="formdelete{{$sale->id}}" action="{{route('sales.destroy', ['id' => $sale->id])}}" method="post">
                                                    {{ csrf_field() }}
                                                    {{ method_field('delete') }}
                                                  </form>
                                                    <button type="button" class="btn btn-danger" id="sa-eliminar{{$sale->id}}">Borrar</button>
                                                    <a href="{{route('sales.show', ['id' => $sale->id])}}" class="btn btn-primary">Ver</a>
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Aceptar</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                        @endforeach
                        @if (App\Sale::count() == 0)
                          <td></td>
                            <td>No se encontraron resultados</td>

                        @endif
                      </tbody>
                  </table>
              </div>
        </div>
      </div>

    @endsection
    @section('scripts')
      <script src="{{asset('assets/plugins/datatables/jquery.dataTables.min.js')}}"></script>
      <!-- start - This is for export functionality only -->
      <script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
      <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
      <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
      <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
      <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
      <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
      <script>
      $(document).ready(function() {
          $('#myTable').DataTable();
          });
          !function($) {
              "use strict";

              var SweetAlert = function() {};

              //examples
              SweetAlert.prototype.init = function() {
              @foreach (Auth::user()->business->sales as $sale)
              $('#sa-eliminar{{$sale->id}}').click(function(){
                  swal({
                      title: "¿Estás seguro?",
                      text: "Eliminarás la venta #{{$sale->name}}",
                      type: "warning",
                      showCancelButton: true,
                      confirmButtonColor: "#DD6B55",
                      confirmButtonText: "¡Borralo!",
                      cancelButtonText: "Cancelar",
                      closeOnConfirm: false
                  }, function(e){
                    if (e) {
                      swal("Eliminado", "La venta fue eliminada.", "success");

                      $('#formdelete{{$sale->id}}').submit();
                      $("#venta{{$sale->id}}").remove();
                    }
                    else {
                      swal("Cancelado", "Tu venta está a salvo.", "success");
                    }


                  });
              });
              @endforeach





              },
              //init
              $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
          }(window.jQuery),

          //initializing
          function($) {
              "use strict";
              $.SweetAlert.init()
          }(window.jQuery);
      </script>
      <script src="{{asset('assets/plugins/sweetalert/sweetalert.min.js')}}"></script>
      <script src="{{asset('assets/plugins/sweetalert/jquery.sweet-alert.custom.js')}}"></script>

    @endsection
