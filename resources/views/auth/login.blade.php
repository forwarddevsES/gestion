@extends('layouts.app')

@section('content')

  <div class="card-body">
      <form class="form-horizontal form-material" method="post" id="loginform" action="{{ route('login') }}">
        @csrf
          <h3 class="box-title m-b-20">Ingreso</h3>
          <div class="form-group ">
              <div class="col-xs-12">
                <input id="email" placeholder="Correo Electronico" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>

                @if ($errors->has('email'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
              </div>
          </div>
          <div class="form-group">
              <div class="col-xs-12">
                <input id="password" placeholder="Contraseña" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                @if ($errors->has('password'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
              </div>
          </div>
          <div class="form-group">
              <div class="col-md-12">
                  <div class="checkbox checkbox-primary pull-left p-t-0">
                    <label>
                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> {{ __('Recuerdame') }}
                    </label>

                  </div>
                  <a class="text-dark pull-right" href="{{ route('password.request') }}">
                      <i class="fa fa-lock m-r-5"></i> {{ __('¿Olvidaste tu contraseña?') }}
                  </a>
              </div>
          </div>
          <div class="form-group text-center m-t-20">
              <div class="col-xs-12">
                  <button class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">{{ __('Ingresar') }}</button>
              </div>
          </div>

      </form>

  </div>


@endsection
