@if (\Session::has('message'))
    <div class="alert alert-success alert-rounded">
        {!! \Session::get('message') !!}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
    </div>
@endif
@if (\Session::has('warning'))
    <div class="alert alert-warning alert-rounded">
        {!! \Session::get('warning') !!}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
    </div>
@endif
@if ($errors->any())
      <div class="alert alert-danger alert-rounded">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
          <ul>
              @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
              @endforeach
          </ul>
      </div>
  @endif
