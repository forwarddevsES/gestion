@extends('panel.layouts.panel')
@section('content')
<div class="row p-20">
    <div class="col-md-4">
        <div class="card">
            <img class="card-img" src="{{asset('assets/images/background/socialbg.jpg')}}" width="200" alt="Card image">
            <div class="card-img-overlay card-inverse social-profile d-flex ">

                <div class="align-self-center text-center"> <img src="{{asset('assets/images/avatar.png')}}" class="img-circle" width="100">
                    <h4 class="card-title">{{Auth::user()->first_name}} {{Auth::user()->last_name}}</h4>
                    <h6 class="card-subtitle">{{Auth::user()->business->name}}</h6>
                    <p class="text-white">{{Auth::user()->business->address}}, {{Auth::user()->business->city}}({{Auth::user()->business->cp}}), {{Auth::user()->business->province}}</p>
                    {{--@php
                    if (Auth::user()->business->activeSubscription()) {
                    $subscription = Auth::user()->business->activeSubscription();
                    }
                    else {
                    $subscription = Auth::user()->business->lastActiveSubscription();
                    }
                    @endphp
                    @if ($subscription->remainingDays())
                    <p class="text-white">Tu plan vence en {{$subscription->remainingDays()}} dias.</p>
                    @else
                    <p style="color:red;">Tu plan esta vencido.</p>
                    @endif--}}
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-body"> <small class="text-muted">Dirección de email</small>
                <h6>{{Auth::user()->business->email}}</h6> <small class="text-muted p-t-30 db">Teléfono</small>
                <h6>{{Auth::user()->business->phone}}</h6> <small class="text-muted p-t-30 db">Dirección</small>
                <h6>{{Auth::user()->business->address}}, {{Auth::user()->business->city}}({{Auth::user()->business->cp}}), {{Auth::user()->business->province}}</h6>

            </div>
        </div>
    </div>
    <div class="col-md-8">
        @if (\Session::has('message'))
        <div class="alert alert-warning alert-rounded">
            {!! \Session::get('message') !!}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
        </div>
        @endif
        <div class="row">

            <div class="col-md-6 col-lg-4 col-xlg-4">
                <a href="{{route('entries.index')}}" class="card card-inverse card-dark">
                    <div class="box text-center">
                        <h2 class="font-light text-white">{{Auth::user()->business->entries->count()}} {{--@if($subscription->features()->code('entries.amount')->first()->limit != -1)/ {{$subscription->features()->code('entries.amount')->first()->limit}} @endif--}}</h2>
                        <h6 class="text-white">Ordenes de trabajo</h6>
                    </div>
                </a>
            </div>
            <div class="col-md-6 col-lg-4 col-xlg-4">
                <a href="{{route('products.index')}}" class="card card-primary card-inverse">
                    <div class="box text-center">
                        <h2 class="font-light text-white">{{Auth::user()->business->products->count()}} {{--@if($subscription->features()->code('products.amount')->first()->limit != -1)/ {{$subscription->features()->code('products.amount')->first()->limit}} @endif--}}</h2>
                        <h6 class="text-white">Productos</h6>
                    </div>
                </a>
            </div>
            <div class="col-md-6 col-lg-4 col-xlg-4">
                <a href="{{route('clients.index')}}" class="card card-success card-inverse">
                    <div class="box text-center">
                        <h2 class="font-light text-white">{{Auth::user()->business->clients->count()}} {{--@if($subscription->features()->code('clients.amount')->first()->limit != -1)/ {{$subscription->features()->code('clients.amount')->first()->limit}} @endif--}}</h2>
                        <h6 class="text-white">Clientes</h6>
                    </div>
                </a>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 col-lg-4 col-xlg-4">
                <a href="{{route('sales.index')}}" class="card card-info card-dark">
                    <div class="box text-center">
                        <h2 class="font-light text-white">
                            {{Auth::user()->business->sales->count()}} {{--@if($subscription->features()->code('sales.amount')->first()->limit != -1)/ {{$subscription->features()->code('sales.amount')->first()->limit}} @endif--}}
                        </h2>
                        <h6 class="text-white">Ventas</h6>
                    </div>
                </a>
            </div>
            <div class="col-md-6 col-lg-4 col-xlg-4">
                <a href="{{route('users.index')}}" class="card card-danger card-inverse">
                    <div class="box text-center">
                        <h2 class="font-light text-white">{{Auth::user()->business->users->count()}} {{--@if($subscription->features()->code('users.amount')->first()->limit != -1)/ {{$subscription->features()->code('users.amount')->first()->limit}} @endif--}}</h2>
                        <h6 class="text-white">Usuarios</h6>
                    </div>
                </a>
            </div>
            <div class="col-md-6 col-lg-4 col-xlg-4">
                <a href="{{route('providers.index')}}" class="card card-warning card-inverse">
                    <div class="box text-center">
                        <h2 class="font-light text-white">{{Auth::user()->business->providers->count()}} {{--@if($subscription->features()->code('providers.amount')->first()->limit != -1)/ {{$subscription->features()->code('providers.amount')->first()->limit}} @endif--}}</h2>
                        <h6 class="text-white">Proveedores</h6>
                    </div>
                </a>
            </div>
        </div>
        <div class="table-responsive m-t-40">
            <div class="card">
                <div class="card-body">
                <table id="myTable" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Cliente</th>
                            <th>Fecha</th>
                            <th>Total</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach (Auth::user()->business->sales as $sale)
                        <tr id="venta{{$sale->id}}" data-toggle="modal" data-target="#infon{{$sale->id}}" data-whatever="@mdo">
                            <td>{{$sale->id}}</td>
                            <td>{{$sale->client->name}}</td>
                            <td>{{$sale->created_at}}</td>
                            <td>$ {{$sale->final_price}}</td>
                        </tr>
                        <div class="modal fade" id="infon{{$sale->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1" style="display: none;" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title" id="exampleModalLabel1">{{$sale->client->name}} (#{{$sale->client->id}})</h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                                    </div>
                                    <div class="modal-body">
                                        @foreach ($sale->items as $item)
                                        <p>{{$item->name}} x {{$item->amount}} $<b>{{$item->price}}</b> </p>
                                        @endforeach

                                    </div>
                                    <div class="modal-footer">
                                        <form id="formdelete{{$sale->id}}" action="{{route('sales.destroy', ['id' => $sale->id])}}" method="post">
                                            {{ csrf_field() }}
                                            {{ method_field('delete') }}
                                        </form>
                                        <button type="button" class="btn btn-danger" id="sa-eliminar{{$sale->id}}">Borrar</button>
                                        <a href="{{route('sales.show', ['id' => $sale->id])}}" class="btn btn-primary">Ver</a>
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Aceptar</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                        {{-- @if (Auth::user()->business->sales->count() == 0)

                        <td colspan="4" style="text-align:center;">No se encontraron resultados</td>

                        @endif --}}
                    </tbody>
                </table>
            </div>
            </div>
        </div>
    </div>
</div>

@endsection
@section('scripts')
<script src="{{asset('assets/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<!-- start - This is for export functionality only -->
<script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
<script>
    $(document).ready(function() {
        $('#myTable').DataTable({
            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
            }
        });
    });
    ! function($) {
        "use strict";

        var SweetAlert = function() {};

        //examples
        SweetAlert.prototype.init = function() {
                @foreach(Auth::user() -> business -> sales as $sale)
                $('#sa-eliminar{{$sale->id}}').click(function() {
                    swal({
                        title: "¿Estás seguro?",
                        text: "Eliminarás la venta #{{$sale->name}}",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "¡Borralo!",
                        cancelButtonText: "Cancelar",
                        closeOnConfirm: false
                    }, function(e) {
                        if (e) {
                            swal("Eliminado", "La venta fue eliminada.", "success");

                            $('#formdelete{{$sale->id}}').submit();
                            $("#venta{{$sale->id}}").remove();
                        } else {
                            swal("Cancelado", "Tu venta está a salvo.", "success");
                        }


                    });
                });
                @endforeach





            },
            //init
            $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
    }(window.jQuery),

    //initializing
    function($) {
        "use strict";
        $.SweetAlert.init()
    }(window.jQuery);
</script>
<script src="{{asset('assets/plugins/sweetalert/sweetalert.min.js')}}"></script>
<script src="{{asset('assets/plugins/sweetalert/jquery.sweet-alert.custom.js')}}"></script>

@endsection
