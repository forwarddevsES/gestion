@extends('panel.layouts.panel')
@section('content')
<br>
    <div class="row">
      <div class="col-lg-12">
        
        <div class="card card-outline-info">
            <div class="card-header">
                <h4 class="m-b-0 text-white text-center">Crear producto</h4>
            </div>
                <div class="card-body">
                    @include('vendor.errors')
                    <form action="{{route('products.store')}}" method="post">
                        {{ csrf_field() }}
                        <div class="form-body">
                            <div class="row p-t-20">

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Detalle</label>
                                        <input type="text" name='name' id="name" class="form-control form-control-danger" placeholder="Auriculares Sony" required>
                                        <small class="form-control-feedback"> Nombre del Producto. </small> </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Codigo</label>
                                        <input type="text" name='code' id="code" class="form-control" placeholder="113327" required>
                                        <small class="form-control-feedback"> Codigo del Producto</small> </div>
                                </div>

                            </div>
                            <div class="row">

                                <div class="col-md-6">
                                    <div class="form-group has-success">
                                        <label class="control-label">Existencias Iniciales</label>
                                        <input type="number" name="amount" class="form-control" placeholder="26" required>
                                        <small class="form-control-feedback"> Cantidad en Stock </small> </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Precio</label>
                                        <input type="number" name="price" class="form-control" min="0.00" max="1000000.00" step="0.01" placeholder="10" required>
                                        <small class="form-control-feedback"> Precio por unidad (Para la venta) </small> </div>
                                </div>

                            </div>
                        </div>
                        <div class="form-actions">
                            <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> Guardar</button>
                            <a href="{{ route('products.index') }}" class="btn btn-danger" data-dismiss="modal" >Cancelar</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
