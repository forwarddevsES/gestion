@extends('panel.layouts.panel')
@section('content')

<div class="card">
  <div class="card-body">
    <ul class="nav nav-tabs customtab" role="tablist">
      <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#" role="tab"><span class="hidden-sm-up"><i class="ti-home"></i></span> <span class="hidden-xs-down">Productos</span></a> </li>
      <li class="nav-item"> <a class="nav-link" href="{{route('providers.index')}}"><span class="hidden-sm-up"><i class="ti-user"></i></span> <span class="hidden-xs-down">Proveedores</span></a> </li>
      <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#messages2" role="tab"><span class="hidden-sm-up"><i class="ti-email"></i></span> <span class="hidden-xs-down">Movimientos de Stock</span></a> </li>
    </ul>
  </div>
</div>

{{-- Main row --}}
<div class="row">

  {{--------------- Inventory list --------------}}
  <div class="col-sm-12 col-md-8">
    <div class="card card-outline-info">

      <div class="card-header">
        <h4 class="m-b-0 text-white text-center">Lista de productos</h4>
      </div>

      <div class="card-body">
        <div class="table-responsive m-t-40">
          <table id="myTable" class="table table-bordered table-hover">
            <thead>
              <tr>
                <th>#</th>
                <th>Detalle</th>
                <th>Existencias</th>
                <th>Precio Unidad</th>
              </tr>
            </thead>
            <tbody>

              @foreach ($products as $product)

              <tr id="producto{{$product->id}}" data-toggle="modal" data-target="#infon{{$product->id}}" data-whatever="@mdo">
                <td>{{$product->code}}</td>
                <td>{{$product->name}}</td>
                <td>{{$product->amount}}</td>
                <td>{{$product->price}}</td>
              </tr>

              {{------------------ Modal -------------}}
              <div class="modal fade" id="infon{{$product->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1" style="display: none;" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h4 class="modal-title" id="exampleModalLabel1">{{$product->name}} (#{{$product->code}})</h4>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    </div>
                    <div class="modal-body">
                      <p>Existencias: <b>{{$product->amount}}</b> </p>
                      <p>Precio: ${{$product->price}}</p>
                    </div>
                    <div class="modal-footer">
                      <form id="formdelete{{$product->id}}" action="{{route('products.destroy', ['id' => $product->id])}}" method="post">
                        {{ csrf_field() }}
                        {{ method_field('delete') }}
                      </form>
                      <button type="button" class="btn btn-danger btn-rounded" id="sa-eliminar{{$product->id}}">Borrar</button>
                      <a href="{{route('products.edit', ['id' => $product->id])}}" class="btn btn-primary btn-rounded">Editar</a>
                      <button type="button" class="btn btn-default btn-rounded" data-dismiss="modal">Aceptar</button>
                    </div>
                  </div>
                </div>
              </div>
              {{----------------------------------}}

              @endforeach
              {{-- @if (Auth::user()->business->products->count() == 0)
              <td colspan="4" style="text-align:center;">No se encontraron resultados</td>
              @endif --}}

            </tbody>
          </table>
        </div>
      </div>

    </div>
  </div>
  {{-- ------------------------------- --}}

  {{-- -------- New product ---------- --}}
  <div class="col-sm-12 col-md-4">
    <div class="card card-outline-info">

      <div class="card-header">
        <h4 class="m-b-0 text-white text-center">Nuevo producto</h4>
      </div>

      <div class="card-body">
        @include('vendor.errors')
        <form action="{{route('products.store')}}" method="post">
          {{ csrf_field() }}

          <div class="form-group">
            <label class="control-label">Detalle</label>
            <input type="text" name='name' id="name" class="form-control form-control-danger" placeholder="Auriculares Sony" required>
            <small class="form-control-feedback"> Nombre del Producto. </small>
          </div>

          <div class="form-group">
            <label class="control-label">Codigo</label>
            <input type="text" name='code' id="code" class="form-control" placeholder="113327" required>
            <small class="form-control-feedback"> Codigo del Producto</small>
          </div>

          <div class="form-group">
            <label class="control-label">Existencias Iniciales</label>
            <input type="number" name="amount" class="form-control" placeholder="26" required>
            <small class="form-control-feedback"> Cantidad en Stock </small>
          </div>

          <div class="form-group">
            <label class="control-label">Precio</label>
            <input type="number" name="price" class="form-control" min="0.00" max="1000000.00" step="0.01" placeholder="10" required>
            <small class="form-control-feedback"> Precio por unidad (Para la venta) </small>
          </div>

          <div class="form-actions">
            <button type="submit" class="btn btn-success btn-block btn-rounded"> <i class="fa fa-check"></i> Guardar</button>
          </div>
        </form>
      </div>

    </div>
  </div>
  {{-- ------------------------------- --}}

</div>
{{-- main row --}}

@endsection

@section('scripts')
<script src="{{asset('assets/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<!-- start - This is for export functionality only -->
<script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
<script>
  $(document).ready(function() {
    $('#myTable').DataTable({
      "language": {
        "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
      }
    });
  });
  ! function($) {
    "use strict";

    var SweetAlert = function() {};

    //examples
    SweetAlert.prototype.init = function() {
        @foreach($products as $product)
        $('#sa-eliminar{{$product->id}}').click(function() {
          swal({
            title: "¿Estás seguro?",
            text: "Eliminarás el producto '{{$product->name}}'",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "¡Borralo!",
            cancelButtonText: "Cancelar",
            closeOnConfirm: false
          }, function(e) {
            if (e) {
              swal("Eliminado", "El producto fue eliminado.", "success");

              $('#formdelete{{$product->id}}').submit();
              $("#producto{{$product->id}}").remove();
            } else {
              swal("Cancelado", "Tu producto está a salvo.", "success");
            }


          });
        });
        @endforeach





      },
      //init
      $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
  }(window.jQuery),

  //initializing
  function($) {
    "use strict";
    $.SweetAlert.init()
  }(window.jQuery);
</script>
<script src="{{asset('assets/plugins/sweetalert/sweetalert.min.js')}}"></script>
<script src="{{asset('assets/plugins/sweetalert/jquery.sweet-alert.custom.js')}}"></script>

@endsection
