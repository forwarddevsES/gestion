@extends('panel.layouts.panel')
@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="card card-outline-info">
            <div class="card-header">
                <h4 class="m-b-0 text-white">Editar producto de proveedor</h4>
            </div>

            <div class="card-body">
                @include('vendor.errors')
                <form action="{{route('items.update', ['id' => $item->id])}}" method="post">
                  {{ csrf_field() }}
                  {{ method_field('PUT')}}
                    <div class="form-body">

                        <h3 class="card-title">Información basica</h3>
                        <hr>
                        <div class="row p-t-20">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Nombre</label>
                                    <input type="text" name='name' id="name" class="form-control" placeholder="Nombre" required value=" {{$item->name}} ">
                                    <small class="form-control-feedback"> Nombre del producto</small> </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                  <label class="control-label">Precio</label>
                                  <input type="number" name="price" id="price" class="form-control" required value="{{$item->price}}">
                                  <small class="form-control-feedback">Precio del producto</small>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row p-t-20">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">Código</label>
                                <input type="number" name="code" id="code" class="form-control" placeholder="54667" value="{{$item->code}}">
                                <small class="form-control-feedback">Código del producto</small>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> Guardar</button>
                        <a href=" {{ route('providers.index') }} " class="btn btn-danger" data-dismiss="modal" >Cancelar</a>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>
@endsection
