@extends('panel.layouts.panel')
@section('content')
<br>
<div class="row">
    <div class="col-lg-12">
        <div class="card card-outline-info">
            <div class="card-header">
                <h4 class="m-b-0 text-white">Crear un nuevo producto de proveedor</h4>
            </div>
            <div class="card-body">
                @include('vendor.errors')
                <form action=" {{ route('items.store') }} " method="post">
                    {{ csrf_field() }}
                    <div class="form-body">
                            <h3 class="card-title">Seleccionar proveedor</h3>
                            <hr>
                        <div class="row">

                            <div class="col-md-6">

                                <center><label class="control-label">Si el proveedor ya esta registrado en la base de datos</label>
                                <select class="select2" name="provider_id" style="width: 100%" required>
                                    <option value="" disabled selected>Seleccione un proveedor</option>
                                    <optgroup label="Proveedores disponibles">
                                        @foreach (Auth::user()->business->providers as $provider)
                                        <option value="{{$provider->id}}">{{$provider->name}} ({{$provider->email}})</option>
                                      @endforeach

                                    </optgroup>

                                </select></center>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <center><label class="control-label">Si el proveedor es nuevo debe registrarlo</label><br></center>
                                    <center><a class="btn btn-rounded btn-success" href="{{route('providers.create')}}"><b style="color:white;">NUEVO PROVEEDOR</b></a></center>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="card-body">
                            <div class="form-body">
                                <hr>
                                <div class="row p-t-20">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Nombre</label>
                                            <input type="text" name='name' id="name" class="form-control" placeholder="Nombre" required >
                                            <small class="form-control-feedback"> Nombre del producto</small>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                          <label class="control-label">Precio</label>
                                          <input type="number" name="price" id="price" class="form-control" required >
                                          <small class="form-control-feedback">Precio del producto</small>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row p-t-20">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Código</label>
                                        <input type="number" name="code" id="code" class="form-control" placeholder="54667" required>
                                        <small class="form-control-feedback">Código del producto</small>
                                    </div>
                                </div>
                            </div>
                            <div class="form-actions">
                                <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> Guardar</button>
                                <a href="{{ route('providers.index') }}" class="btn btn-danger" data-dismiss="modal" >Cancelar</a>
                            </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>
@endsection
