@extends('panel.layouts.panel')
@section('content')
  <div class="card">
    <div class="card-body">
      <ul class="nav nav-tabs customtab" role="tablist">
        <li class="nav-item"> <a class="nav-link" href="{{route('products.index')}}"><span class="hidden-sm-up"><i class="ti-home"></i></span> <span class="hidden-xs-down">Productos</span></a> </li>
        <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#" role="tab"><span class="hidden-sm-up"><i class="ti-user"></i></span> <span class="hidden-xs-down">Proveedores</span></a> </li>
        <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#messages2" role="tab"><span class="hidden-sm-up"><i class="ti-email"></i></span> <span class="hidden-xs-down">Movimientos de Stock</span></a> </li>
      </ul>
    </div>
  </div>
{{-- Main row --}}
<div class="row">

  {{----------- Provider list -------------------}}
  <div class="col-sm-12 col-md-8">
    <div class="card card-outline-info">

      <div class="card-header">
          <h4 class="m-b-0 text-white text-center">Lista de proveedores</h4>
      </div>

      <div class="card-body">
        <div class="table-responsive m-t-40">
          <table id="myTable" class="table table-bordered table-hover">
            <thead>
              <tr>
                <th>#</th>
                <th>Nombre</th>
                <th>Dirección</th>
                <th>Telefono</th>
                <th>Email</th>
                <th>Información</th>
              </tr>
            </thead>
            <tbody>
              @foreach (Auth::user()->business->providers as $provider)

                  <tr class='clickable-row' data-href='{{route('providers.show', ['id' => $provider->id])}}'>
                        <td>{{$provider->id}}</td>
                        <td>{{$provider->name}}</td>
                        <td>{{$provider->address}}</td>
                        <td>{{$provider->phone}}</td>
                        <td>{{$provider->email}}</td>
                        <td>{{$provider->info}}</td>
                  </tr>

              {{-- <tr id="provider{{$provider->id}}" data-toggle="modal" data-target="#infon{{$provider->id}}" data-whatever="@mdo">
                  <td>{{$provider->id}}</td>
                  <td>{{$provider->name}}</td>
                  <td>{{$provider->address}}</td>
                  <td>{{$provider->phone}}</td>
                  <td>{{$provider->email}}</td>
                  <td>{{$provider->info}}</td>
                </tr> --}}

              <!-------------------------------------------- Modal -------------------------------------------------------->
              {{-- <div class="modal fade" id="infon{{$provider->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1" style="display: none;" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h4 class="modal-title" id="exampleModalLabel1">{{$provider->name}} (#{{$provider->id}})</h4>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    </div>
                    <div class="modal-body">
                      <p>Nombre: <b>{{$provider->name}}</b> </p>
                      <p>Dirección: <b>{{$provider->address}}</b> </p>
                      <p>Teléfono: <b>{{$provider->phone}}</b> </p>
                      <p>Email: <b>{{$provider->email}}</b> </p>
                      <p>Info: <b>{{$provider->info}}</b> </p>
                      <p>Productos del proveedor: <a href=" {{route('providers.show', ['id' => $provider->id])}} " class="btn btn-sm btn-primary">Ver</a> </p>
                    </div>
                    <div class="modal-footer">
                      <form id="formdelete{{$provider->id}}" action="{{route('providers.destroy', ['id' => $provider->id])}}" method="post">
                        {{ csrf_field() }}
                        {{ method_field('delete') }}
                      </form>
                      <button type="button" class="btn btn-danger" id="sa-eliminar{{$provider->id}}">Borrar</button>
                      <a href="{{route('providers.edit', ['id' => $provider->id])}}" class="btn btn-primary">Editar</a>
                      <button type="button" class="btn btn-default" data-dismiss="modal">Aceptar</button>
                    </div>
                  </div>
                </div>
              </div> --}}
              <!---------------------------------------------------------------------------------------------------->
              @endforeach
              {{-- @if (Auth::user()->business->providers->count() == 0)
              <td colspan="6" class="text-center">No se encontraron resultados</td>
              @endif --}}
            </tbody>
          </table>
        </div>
      </div>

    </div>
  </div>
  {{-------------------------------------------------------------------}}

  {{------------------------ New provider  ----------------------------}}
  <div class="col-sm-12 col-md-4">
    <div class="card card-outline-info">

      <div class="card-header">
          <h4 class="m-b-0 text-white text-center">Nuevo proveedor</h4>
      </div>

      <div class="card-body">
        @include('vendor.errors')
        <form action="{{route('providers.store')}}" method="post">
          {{ csrf_field() }}

          <div class="form-group">
            <label class="control-label">Nombre</label>
            <input type="text" name='name' id="name" class="form-control" placeholder="Nombre" required>
            <small class="form-control-feedback"> Nombre del proveedor</small>
          </div>

          <div class="form-group">
            <label class="control-label">Email</label>
            <input type="text" name='email' id="email" class="form-control form-control-danger" placeholder="proveedor@gmail.com" required>
            <small class="form-control-feedback"> Correo electrónico del proveedor </small>
          </div>

          <div class="form-group">
            <label class="control-label">Dirección</label>
            <input type="text" name="address" id="address" class="form-control" placeholder="Calle Numero, Ciudad(CP)" required>
            <small class="form-control-feedback"> Direccion del proveedor </small>
          </div>

          <div class="form-group">
            <label class="control-label">Teléfono</label>
            <input type="text" name="phone" id="phone" class="form-control" placeholder="1166778899" required>
            <small class="form-control-feedback"> Numero de telefono del proveedor </small>
          </div>

          <div class="form-group">
            <label class="control-label"> Información adicional </label>
            <textarea name="info" id="info" rows="4" class="form-control" required></textarea>
          </div>

          <button type="submit" class="btn btn-success btn-block btn-rounded"> <i class="fa fa-check"></i> Guardar</button>

        </form>
      </div>

    </div>
  </div>
  {{-- ---------------------------------------------- --}}

</div>
{{-- main row --}}

@endsection

@section('scripts')
<script src="{{asset('assets/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<!-- start - This is for export functionality only -->
<script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
<script>
  $(document).ready(function() {
    $('#myTable').DataTable({
      "language": {
        "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
      }
    });
  });
  ! function($) {
    "use strict";

    var SweetAlert = function() {};

    //examples
    SweetAlert.prototype.init = function() {
        @foreach(Auth::user() -> business -> providers as $provider)
        $('#sa-eliminar{{$provider->id}}').click(function() {
          swal({
            title: "¿Estás seguro?",
            text: "Eliminarás el proveedor '{{$provider->name}}'",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "¡Borralo!",
            cancelButtonText: "Cancelar",
            closeOnConfirm: false
          }, function(e) {
            if (e) {
              swal("Eliminado", "El proveedor fue eliminado.", "success");

              $('#formdelete{{$provider->id}}').submit();
              $("#providero{{$provider->id}}").remove();
            } else {
              swal("Cancelado", "Tu proveedor está a salvo.", "success");
            }


          });
        });
        @endforeach





      },
      //init
      $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
  }(window.jQuery),

  //initializing
  function($) {
    "use strict";
    $.SweetAlert.init()
  }(window.jQuery);
</script>

<script src="{{asset('assets/plugins/sweetalert/sweetalert.min.js')}}"></script>
<script src="{{asset('assets/plugins/sweetalert/jquery.sweet-alert.custom.js')}}"></script>

@endsection
