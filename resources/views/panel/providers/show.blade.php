@extends('panel.layouts.panel')
@section('content')
<br>
{{----------- Provider info -----------------}}
<div class="card card-outline-info">

  <div class="card-header">
    <h4 class="m-b-0 text-white text-center">Información del proveedor</h4>
  </div>

  <div class="card-body">
      <p><b>Nombre: </b>{{$provider->name}}</p>
      <p><b>Dirección: </b>{{$provider->address}}</p>
      <p><b>Telefono: </b>{{$provider->phone}}</p>
      <p><b>Email: </b>{{$provider->email}}</p>
      <p><b>Información adicional: </b>{{$provider->info}}</p>
    <hr>
    <form id="delete-{{ $provider->id }}" action="{{ route('providers.destroy', $provider->id) }}" method="POST">
      {{ csrf_field() }}
      {{ method_field('delete') }}
    </form>
    <a href="javascript: document.getElementById('delete-{{ $provider->id }}').submit()" class="btn btn-danger">Borrar</a>

    <a href="{{route('providers.edit', ['id' => $provider->id])}}" class="btn btn-primary">Editar</a>
    <a href="{{route('providers.index')}}" class="btn btn-info">Volver</a>
  </div>
</div>
{{-- --------------- --}}

{{--------------- Provider products ----------}}
<div class="card card-outline-info">
  <div class="card-header">
          <h4 class="m-b-0 text-white text-center">Productos del proveedor</h4>
        </div>
  <div class="card-body">
<br>
    <h5 class="card-subtitle text-center">Si usted desea crear un nuevo producto de proveedor haga <a href=" {{ route('items.create') }} ">click aquí</a>.</h5>
    <div class="table-responsive m-t-40">
      <table id="myTable" class="table table-bordered table-hover">
        <thead>
          <tr>
            <th>#</th>
            <th>Nombre</th>
            <th>Código</th>
            <th>Precio</th>
          </tr>
        </thead>
        <tbody>
          @foreach ($provider->items as $item)


          <tr id="item{{$item->id}}" data-toggle="modal" data-target="#infon{{$item->id}}" data-whatever="@mdo">
            <td>{{$item->id}}</td>
            <td>{{$item->name}}</td>
            <td>{{$item->code}}</td>
            <td>{{$item->price}}</td>
          </tr>
          <!--------------------- Modal ------------------------>
          <div class="modal fade" id="infon{{$item->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1" style="display: none;" aria-hidden="true">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h4 class="modal-title" id="exampleModalLabel1">{{$item->name}} (#{{$item->id}})</h4>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body">
                  <p>Nombre: <b>{{$item->name}}</b> </p>
                  <p>Código: <b>{{$item->code}}</b> </p>
                  <p>Precio: <b>{{$item->price}}</b> </p>
                </div>
                <div class="modal-footer">
                  <form id="formdelete{{$item->id}}" action="{{route('items.destroy', ['id' => $item->id])}}" method="post">
                    {{ csrf_field() }}
                    {{ method_field('delete') }}
                  </form>
                  <button type="button" class="btn btn-danger" id="sa-eliminar{{$item->id}}">Borrar</button>
                  <a href="{{route('items.edit', ['id' => $item->id])}}" class="btn btn-primary">Editar</a>
                  <button type="button" class="btn btn-default" data-dismiss="modal">Aceptar</button>
                </div>
              </div>
            </div>
          </div>
          <!----------------------------------------------------->
          @endforeach
          @if ($provider->items->count() == 0)
          <td colspan="4" class="text-center">No se encontraron resultados</td>
          @endif
        </tbody>
      </table>

    </div>
  </div>
</div>
{{-- ----------------------- --}}

@endsection

@section('scripts')
<script src="{{asset('assets/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<!-- start - This is for export functionality only -->
<script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
<script>
  $(document).ready(function() {
    $('#myTable').DataTable({
      "language": {
        "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
      }
    });
  });
  ! function($) {
    "use strict";

    var SweetAlert = function() {};

    //examples
    SweetAlert.prototype.init = function() {
        @foreach($provider -> items as $item)
        $('#sa-eliminar{{$item->id}}').click(function() {
          swal({
            title: "¿Estás seguro?",
            text: "Eliminarás el producto de proveedor '{{$item->name}}'",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "¡Borralo!",
            cancelButtonText: "Cancelar",
            closeOnConfirm: false
          }, function(e) {
            if (e) {
              swal("Eliminado", "El producto de proveedor fue eliminado.", "success");

              $('#formdelete{{$item->id}}').submit();
              $("#itemo{{$item->id}}").remove();
            } else {
              swal("Cancelado", "Tu producto de proveedor está a salvo.", "success");
            }


          });
        });
        @endforeach

      },
      //init
      $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
  }(window.jQuery),

  //initializing
  function($) {
    "use strict";
    $.SweetAlert.init()
  }(window.jQuery);
</script>
<script src="{{asset('assets/plugins/sweetalert/sweetalert.min.js')}}"></script>
<script src="{{asset('assets/plugins/sweetalert/jquery.sweet-alert.custom.js')}}"></script>

@endsection