@extends('panel.layouts.panel')
@section('content')
<br>
<div class="row">
    <div class="col-lg-12">
        <div class="card card-outline-info">
            <div class="card-header">
                <h4 class="m-b-0 text-white text-center">Crear proveedor</h4>
            </div>
            <div class="card-body">
                @include('vendor.errors')
                <form action="{{route('providers.store')}}" method="post">
                  {{ csrf_field() }}
                    <div class="form-body">
                        <div class="row p-t-20">

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Nombre</label>
                                    <input type="text" name='name' id="name" class="form-control" placeholder="Nombre" required>
                                    <small class="form-control-feedback"> Nombre del proveedor</small> </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Email</label>
                                    <input type="text" name='email' id="email" class="form-control form-control-danger" placeholder="proveedor@gmail.com" required>
                                    <small class="form-control-feedback"> Correo electrónico del proveedor </small> </div>
                            </div>

                        </div>

                        <div class="row p-t-20">

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Dirección</label>
                                    <input type="text" name="address" id="address" class="form-control" placeholder="Calle Numero, Ciudad(CP)" required>
                                    <small class="form-control-feedback"> Direccion del proveedor </small> </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                  <label class="control-label">Teléfono</label>
                                  <input type="text" name="phone" id="phone" class="form-control" placeholder="1166778899" required>
                                  <small class="form-control-feedback"> Numero de telefono del proveedor </small> </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label"> Información adicional </label>
                                <textarea name="info" id="info" rows="7" class="form-control" required ></textarea>
                            </div>

                    </div>

                        <div class="form-actions">
                            <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> Guardar</button>
                            <a href="{{ route('providers.index') }}" class="btn btn-danger" data-dismiss="modal" >Cancelar</a>
                        </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
