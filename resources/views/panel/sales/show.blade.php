@extends('panel.layouts.panel')
@section('content')
  <div class="row" id="printableArea">
      <div class="col-md-12">

          <div class="card card-body printableArea">
              <h3><b>DOCUMENTO NO VALIDO COMO FACTURA</b> <span class="pull-right">#{{$sale->id}}</span></h3>
              <hr>
              <div class="row">
                  <div class="col-md-12">
                      <div class="pull-left">
                        <address>
                            <h3> &nbsp;<b class="text-danger">{{Auth::user()->business->name}}</b></h3>
                            <p class="text-muted m-l-5">{{Auth::user()->business->address}}
                                <br/> {{Auth::user()->business->city}} ({{Auth::user()->business->cp}})
                                <br/> {{Auth::user()->business->province}}
                                <br/> Tel.: {{Auth::user()->business->phone}}</p>
                        </address>
                      </div>
                      <div class="pull-right text-right">
                          <address>
                              <h3>Para,</h3>
                              <h4 class="font-bold">{{$sale->client->name}}</h4>
                              <p class="text-muted m-l-30">{{$sale->client->address}},
                                  <br/> Tel.: {{$sale->client->phone}}</p>
                              <p class="m-t-30"><b>Fecha de emisión :</b> <i class="fa fa-calendar"></i> {{$sale->created_at}}</p>
                          </address>
                      </div>
                  </div>
                  <div class="col-md-12">

                      <table class="table">
                        <thead>
                          <tr>
                            <th>#</th>
                            <th>Producto</th>
                            <th>Cantidad</th>
                            <th>Sub-Total</th>
                          </tr>
                        </thead>
                        <tbody>
                          @php
                            $totalprice = 0;
                          @endphp
                          @foreach ($sale->items as $item)
                            <tr>
                              <td>{{$item->code}}</td>
                              <td>{{$item->name}}</td>
                              <td>{{$item->amount}}</td>
                              <td>${{$item->price*$item->amount}}</td>
                              </tr>
                              @php
                                $totalprice = $totalprice+($item->price*$item->amount);
                              @endphp
                          @endforeach
                          <td></td>
                        </tbody>
                      </table>
                      <span class="pull-right"><h3><b>Total :</b> ${{$totalprice}}</h3></span>
                  </div>





              </div>
          </div>
      </div>
  </div>
  <div class="clearfix"></div>
  <hr>
  <div class="card card-body">
    <div class="text-right">

        <button onclick="printDiv('printableArea')" class="btn btn-default btn-outline" type="button"> <span><i class="fa fa-print"></i> Imprimir</span> </button>
    </div>
  </div>

@endsection
