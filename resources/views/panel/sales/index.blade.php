@extends('panel.layouts.panel')
@section('content')
  <div class="card card-outline-info">
    <div class="card-body">
      <ul class="nav nav-tabs customtab" role="tablist">
        <li class="nav-item"> <a class="nav-link active"  data-toggle="tab" href="#" role="tab"><span class="hidden-sm-up"><i class="ti-home"></i></span> <span class="hidden-xs-down">Ventas</span></a> </li>
        <li class="nav-item"> <a class="nav-link" href="{{route('clients.index')}}"><span class="hidden-sm-up"><i class="ti-user"></i></span> <span class="hidden-xs-down">Clientes</span></a> </li>
      </ul>
    </div>
  </div>
  <div class="card card-outline-info">
    <div class="card-header">
        <h4 class="m-b-0 text-white text-center">Ventas</h4>
    </div>
      <div class="card-body">
          @include('vendor.errors')
        @include('panel.sections.new', [
          'name' => '',
          'redirect' => route('sales.create'),
          'button' => 'Nueva Venta'
        ])
          <div class="table-responsive m-t-40">
              <table id="myTable" class="table table-bordered table-hover">
                  <thead>
                    <tr>
                        <th>#</th>
                        <th>Cliente</th>
                        <th>Fecha</th>
                        <th>Total</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach ($sales as $sale)
                      <tr id="venta{{$sale->id}}" data-toggle="modal" data-target="#infon{{$sale->id}}" data-whatever="@mdo">
                          <td>{{$sale->id}}</td>
                          <td>{{$sale->client->name}}</td>
                          <td>{{$sale->created_at}}</td>
                          <td>$ {{$sale->final_price}}</td>
                      </tr>
                      <div class="modal fade" id="infon{{$sale->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1" style="display: none;" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title" id="exampleModalLabel1">{{$sale->client->name}} (#{{$sale->client->id}})</h4>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                                            </div>
                                            <div class="modal-body">
                                              @foreach ($sale->items as $item)
                                                <p>{{$item->name}} x {{$item->amount}} $<b>{{$item->price}}</b> </p>
                                              @endforeach

                                            </div>
                                            <div class="modal-footer">
                                              <form id="formdelete{{$sale->id}}" action="{{route('sales.destroy', ['id' => $sale->id])}}" method="post">
                                                {{ csrf_field() }}
                                                {{ method_field('delete') }}
                                              </form>
                                                <button type="button" class="btn btn-danger" id="sa-eliminar{{$sale->id}}">Borrar</button>
                                                <a href="{{route('sales.show', ['id' => $sale->id])}}" class="btn btn-primary">Ver</a>
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Aceptar</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                    @endforeach
                    {{-- @if (Auth::user()->business->sales->count() == 0)
                      <tr>
                        <td colspan="4" style="text-align:center;">No se encontraron resultados</td>
                      </tr>


                    @endif --}}
                  </tbody>
              </table>
          </div>
      </div>
  </div>

@endsection

@section('scripts')
  <script src="{{asset('assets/plugins/datatables/jquery.dataTables.min.js')}}"></script>
  <!-- start - This is for export functionality only -->
  <script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
  <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
  <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
  <script>
  $(document).ready(function() {
      $('#myTable').DataTable({
        "language": {
          "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
        }
      });
  });
      !function($) {
          "use strict";

          var SweetAlert = function() {};

          //examples
          SweetAlert.prototype.init = function() {
          @foreach ($sales as $sale)
          $('#sa-eliminar{{$sale->id}}').click(function(){
              swal({
                  title: "¿Estás seguro?",
                  text: "Eliminarás la venta #{{$sale->name}}",
                  type: "warning",
                  showCancelButton: true,
                  confirmButtonColor: "#DD6B55",
                  confirmButtonText: "¡Borralo!",
                  cancelButtonText: "Cancelar",
                  closeOnConfirm: false
              }, function(e){
                if (e) {
                  swal("Eliminado", "La venta fue eliminada.", "success");

                  $('#formdelete{{$sale->id}}').submit();
                  $("#venta{{$sale->id}}").remove();
                }
                else {
                  swal("Cancelado", "Tu venta está a salvo.", "success");
                }


              });
          });
          @endforeach





          },
          //init
          $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
      }(window.jQuery),

      //initializing
      function($) {
          "use strict";
          $.SweetAlert.init()
      }(window.jQuery);
  </script>
  <script src="{{asset('assets/plugins/sweetalert/sweetalert.min.js')}}"></script>
  <script src="{{asset('assets/plugins/sweetalert/jquery.sweet-alert.custom.js')}}"></script>

@endsection
