@extends('panel.layouts.panel')
@section('content')
<br>
<div class="row">
  <div class="col-lg-12">
    <div class="card card-outline-info">
      <div class="card-header">
        <h4 class="m-b-0 text-white text-center">Nueva venta</h4>
      </div>
      <div class="card-body">
        @include('vendor.errors')
        <form action="{{route('sales.store')}}" class="repeater" method="post">
          {{ csrf_field() }}
          <div class="form-body">

            <h3 class="card-title">Cliente</h3>
            <hr>
            <div class="row">
              <div class="col-md-6">
                <center>
                  <label class="control-label">Si el cliente ya esta registrado en la base de datos</label>
                  <select class="select2" name="client_id" style="width: 100%" required>

                    <optgroup label="Clientes disponibles">
                      @foreach (Auth::user()->business->clients as $clients)
                      <option value="{{$clients->id}}">{{$clients->name}} ({{$clients->email}})</option>
                      @endforeach

                    </optgroup>

                  </select>
                </center>
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <center><label class="control-label">Si el cliente es nuevo debe registrarlo</label><br></center>
                  <center><a class="btn btn-rounded btn-success" href="{{route('clients.create')}}">Nuevo cliente</a></center>
                </div>
              </div>
            </div>
            <h3 class="card-title">Productos</h3>
            <hr>
            @if (!Auth::user()->business->products->count() )

            <h4 class="card-title text-center">No hay productos disponibles </h4>
            <hr>
            @else
            <div class="row">
              {{-- @if (!Auth::user()->business->products->count() )
                              
                                <h4 class="card-title text-center">No hay productos disponibles </h4>
                              
                            @else --}}
              <div data-repeater-list="products" class="col-md-10">
                <div data-repeater-item class="row">
                  <input type="hidden" name="id" id="cat-id" />
                  <div class="col-md-5">
                    <label class="control-label">Producto</label>
                    <select class="select2" name="product" style="width: 100%" required>

                      <optgroup label="Productos">
                        @foreach (Auth::user()->business->products as $product)
                        <option value="{{$product->id}}">{{$product->name}} @if ($product->amount <= 0) (SIN STOCK) @endif </option> @endforeach </optgroup> </select> </div> <div class="col-md-5">
                            <div class="form-group">
                              <label class="control-label">Cantidad</label>
                              <input type="number" min="0" value="1" name="amount" class="form-control" required>
                            </div>
                  </div>


                  <div class="col-md-2">
                    <label class="control-label">Borrar</label><br>
                    <input data-repeater-delete class="btn btn-danger" type="button" value="Borrar" />
                  </div>
                </div>
              </div>
              <div class="col-md-2">
                <label class="control-label">Agregar</label><br>
                <button type="button" data-repeater-create class="btn btn-success" onclick='setTimeout(refreshSelect, 1)'>+</button>
              </div>
              @endif
              
            </div>
<div class="form-actions">
                @if (!Auth::user()->business->products->count())
                <button type="submit" class="btn btn-primary" disabled> <i class="fa fa-check"></i> Guardar</button>
                @else
                <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> Guardar</button>
                @endif

                <a href="{{ route('sales.index') }}" class="btn btn-danger" data-dismiss="modal">Cancelar</a>
              </div>
          </div>



        </form>
      </div>
    </div>
  </div>
</div>

@endsection