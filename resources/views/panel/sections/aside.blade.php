<aside class="left-sidebar">
    <div class="scroll-sidebar">
        <nav class="sidebar-nav">
            <ul id="sidebarnav">
                <li class="nav-small-cap">PERSONAL</li>
                <li>
                    <a href="{{url('panel')}}"><i class="mdi mdi-gauge"></i>Resumen</a>
                </li>
                <li>
                    <a href="{{route('entries.index')}}"><i class="mdi mdi-arrow-right-bold"></i>Ordenes de trabajo</a>
                </li>
                <li>
                    <a href="{{route('sales.index')}}"><i class="mdi mdi-arrow-right-bold"></i>Ventas</a>
                </li>
                <li>
                    <a href="{{route('products.index')}}"><i class="mdi mdi-arrow-right-bold"></i>Inventario</a>
                </li>
                <li>
                    <a href="{{route('users.index')}}"><i class="mdi mdi-account-multiple"></i>Usuarios</a>
                </li>
            </ul>
        </nav>
    </div>
</aside>
