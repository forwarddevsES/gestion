@extends('panel.layouts.panel')
@section('content')
  <div class="card">
    <div class="card-body">
      <ul class="nav nav-tabs customtab" role="tablist">
        <li class="nav-item"> <a class="nav-link" href="{{route('sales.index')}}"><span class="hidden-sm-up"><i class="ti-home"></i></span> <span class="hidden-xs-down">Ventas</span></a> </li>
        <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#" role="tab"><span class="hidden-sm-up"><i class="ti-user"></i></span> <span class="hidden-xs-down">Clientes</span></a> </li>
      </ul>
    </div>
  </div>
{{-- Main row --}}
<div class="row">

  {{-- Client list --}}
  <div class="col-sm-12 col-md-8">
    <div class="card card-outline-info">

      <div class="card-header">
        <h4 class="m-b-0 text-white text-center">Lista de clientes</h4>
      </div>

      <div class="card-body">
        <div class="table-responsive m-t-40">
          <table id="myTable" class="table table-bordered table-hover">
            <thead>
              <tr>
                <th>#</th>
                <th>Nombre</th>
                <th>Email</th>
                <th>Telefono</th>
              </tr>
            </thead>
            <tbody>
              @foreach (Auth::user()->business->clients as $client)

              <tr id="cliente{{$client->id}}" data-toggle="modal" data-target="#infon{{$client->id}}" data-whatever="@mdo">
                <td>{{$client->id}}</td>
                <td>{{$client->name}}</td>
                <td>{{$client->email}}</td>
                <td>{{$client->phone}}</td>
              </tr>

              {{------------- Modal ---------}}
              <div class="modal fade" id="infon{{$client->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1" style="display: none;" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h4 class="modal-title" id="exampleModalLabel1">{{$client->name}} (#{{$client->id}})</h4>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    </div>
                    <div class="modal-body">
                      <p>Nombre: <b>{{$client->name}}</b> </p>
                      <p>Email: <b>{{$client->email}}</b> </p>
                      <p>Dirección: <b>{{$client->address}}</b> </p>
                      <p>Teléfono: <b>{{$client->phone}}</b> </p>

                    </div>
                    <div class="modal-footer">
                      <form id="formdelete{{$client->id}}" action="{{route('clients.destroy', ['id' => $client->id])}}" method="post">
                        {{ csrf_field() }}
                        {{ method_field('delete') }}
                      </form>
                      <button type="button" class="btn btn-danger btn-rounded" id="sa-eliminar{{$client->id}}">Borrar</button>
                      <a href="{{route('clients.edit', ['id' => $client->id])}}" class="btn btn-primary btn-rounded">Editar</a>
                      <button type="button" class="btn btn-default btn-rounded" data-dismiss="modal">Aceptar</button>
                    </div>
                  </div>
                </div>
              </div>
              {{-- --------------- --}}

              @endforeach
              {{-- @if (Auth::user()->business->clients->count() == 0)
              <tr>
                <td colspan="4" style="text-align:center;">No se encontraron resultados</td>
              </tr>
              @endif --}}
            </tbody>
          </table>
        </div>
      </div>

    </div>
  </div>
  {{-- ------------------------- --}}

  {{---------------------- New client ---------------- --}}
  <div class="col-sm-12 col-md-4">
    <div class="card card-outline-info">

      <div class="card-header">
        <h4 class="m-b-0 text-white text-center">Nuevo cliente</h4>
      </div>

      <div class="card-body">
        @include('vendor.errors')
        <form action="{{route('clients.store')}}" method="post">
          {{ csrf_field() }}

          <div class="form-group">
            <label class="control-label">Nombre</label>
            <input type="text" name='name' id="name" class="form-control" placeholder="Nombre Apellido" required>
            <small class="form-control-feedback"> Nombre y Apellido del cliente</small>
          </div>

          <div class="form-group">
            <label class="control-label">Email</label>
            <input type="text" name='email' id="email" class="form-control form-control-danger" placeholder="email@cliente.com">
            <small class="form-control-feedback"> Correo electrónico del cliente </small>
          </div>

          <div class="form-group">
            <label class="control-label">Dirección</label>
            <input type="text" name="address" class="form-control" placeholder="Calle Numero, Ciudad(CP)">
            <small class="form-control-feedback"> Direccion del cliente </small>
          </div>

          <div class="form-group">
            <label class="control-label">Teléfono</label>
            <input type="text" name="phone" class="form-control" placeholder="1166778899">
            <small class="form-control-feedback"> Numero de telefono del cliente </small>
          </div>

          <div class="form-actions">
            <button type="submit" class="btn btn-success btn-block btn-rounded "> <i class="fa fa-check"></i> Guardar</button>
          </div>
        </form>
      </div>

    </div>
  </div>
</div>
{{-- ----------------------------------------------- --}}
</div>
{{-- main row --}}

@endsection

@section('scripts')
<script src="{{asset('assets/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<!-- start - This is for export functionality only -->
<script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
<script>
  $(document).ready(function() {
    $('#myTable').DataTable({
      "language": {
        "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
      }
    });
  });
  ! function($) {
    "use strict";

    var SweetAlert = function() {};

    //examples
    SweetAlert.prototype.init = function() {
        @foreach(Auth::user() -> business -> clients as $client)
        $('#sa-eliminar{{$client->id}}').click(function() {
          swal({
            title: "¿Estás seguro?",
            text: "Eliminarás el cliente '{{$client->name}}', también sus ventas y ordenes.",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "¡Borralo!",
            cancelButtonText: "Cancelar",
            closeOnConfirm: false
          }, function(e) {
            if (e) {
              swal("Eliminado", "El cliente fue eliminado.", "success");

              $('#formdelete{{$client->id}}').submit();
              $("#cliento{{$client->id}}").remove();
            } else {
              swal("Cancelado", "Tu cliente está a salvo.", "success");
            }


          });
        });
        @endforeach





      },
      //init
      $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
  }(window.jQuery),

  //initializing
  function($) {
    "use strict";
    $.SweetAlert.init()
  }(window.jQuery);
</script>
<script src="{{asset('assets/plugins/sweetalert/sweetalert.min.js')}}"></script>
<script src="{{asset('assets/plugins/sweetalert/jquery.sweet-alert.custom.js')}}"></script>

@endsection
