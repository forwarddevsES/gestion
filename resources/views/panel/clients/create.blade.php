@extends('panel.layouts.panel')
@section('content')
<br>
  <div class="row">
      <div class="col-lg-12">
          <div class="card card-outline-info">
              <div class="card-header">
                  <h4 class="m-b-0 text-white text-center">Crear cliente</h4>
              </div>
              <div class="card-body">
                  @include('vendor.errors')
                  <form action="{{route('clients.store')}}" method="post">
                    {{ csrf_field() }}
                      <div class="form-body">
                          <div class="row p-t-20">
                              <div class="col-md-6">
                                  <div class="form-group">
                                      <label class="control-label">Nombre</label>
                                      <input type="text" name='name' id="name" class="form-control" placeholder="Nombre Apellido" required>
                                      <small class="form-control-feedback"> Nombre y Apellido del cliente</small> </div>
                              </div>

                              <div class="col-md-6">
                                  <div class="form-group">
                                      <label class="control-label">Email</label>
                                      <input type="text" name='email' id="email" class="form-control form-control-danger" placeholder="email@cliente.com" >
                                      <small class="form-control-feedback"> Correo electrónico del cliente </small> </div>
                              </div>

                          </div>

                          <div class="row">
                              <div class="col-md-6">
                                  <div class="form-group">
                                      <label class="control-label">Dirección</label>
                                      <input type="text" name="address" class="form-control" placeholder="Calle Numero, Ciudad(CP)" >
                                      <small class="form-control-feedback"> Direccion del cliente </small> </div>
                              </div>

                              <div class="col-md-6">
                                  <div class="form-group">
                                    <label class="control-label">Teléfono</label>
                                    <input type="text" name="phone" class="form-control" placeholder="1166778899" >
                                    <small class="form-control-feedback"> Numero de telefono del cliente </small> </div>
                                  </div>
                              </div>

                          </div>


                      <div class="form-actions">
                          <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> Guardar</button>
                          <a href="{{ route('clients.index') }}" class="btn btn-danger" data-dismiss="modal" >Cancelar</a>
                      </div>
                  </form>
              </div>
          </div>
      </div>
  </div>
@endsection
