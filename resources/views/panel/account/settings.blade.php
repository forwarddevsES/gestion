@extends('panel.layouts.panel')
@section('content')
  <div class="row">
      <!-- Column -->
      <div class="col-lg-4 col-xlg-3 col-md-5">
          <div class="card">
              <div class="card-body">
                  <center class="m-t-30"> <img src="{{asset('assets/images/avatar.png')}}" class="img-circle" width="150" />
                      <h4 class="card-title m-t-10">{{Auth::user()->first_name}} {{Auth::user()->last_name}}</h4>
                      <h6 class="card-subtitle">{{Auth::user()->business->name}}</h6>

                  </center>
              </div>
              <div>
                  <hr> </div>
              <div class="card-body"> <small class="text-muted">Email </small>
                  <h6>{{Auth::user()->email}}</h6> <small class="text-muted p-t-30 db">Teléfono</small>
                  <h6>{{Auth::user()->phone}}</h6> <small class="text-muted p-t-30 db">Domicilio</small>
                  <h6>{{Auth::user()->business->address}}, {{Auth::user()->business->city}} ({{Auth::user()->business->cp}}), {{Auth::user()->business->province}}</h6>

              </div>
          </div>
      </div>
      <!-- Column -->
      <!-- Column -->
      <div class="col-lg-8 col-xlg-9 col-md-7">
          <div class="card">
            @if (\Session::has('message'))
                <div class="alert alert-warning alert-rounded">
                    {!! \Session::get('message') !!}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
                </div>
            @endif
              <!-- Nav tabs -->
              <ul class="nav nav-tabs profile-tab" role="tablist">
                  <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#ajustes" role="tab">Ajustes</a> </li>
                  @if (Auth::user()->group_id == 2)
                    <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#negocio" role="tab">Negocio</a> </li>
                    <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#plan" role="tab">Plan</a> </li>
                  @endif


              </ul>
              <!-- Tab panes -->
              <div class="tab-content">
                  <div class="tab-pane active" id="ajustes" role="tabpanel">
                    <div class="card-body">
                        <form class="form-horizontal form-material" method="post" action="{{route('changeuser', ['user' => Auth::user()])}}">
                          {{ csrf_field() }}
                            <div class="form-group">
                                <label class="col-md-12">Nombre</label>
                                <div class="col-md-12">
                                    <input type="text" name="first_name" placeholder="{{Auth::user()->first_name}}" value="{{Auth::user()->first_name}}" class="form-control form-control-line" required>
                                </div>
                            </div>

                            <div class="form-group">
                              <label class="col-md-12">Apellido</label>
                              <div class="col-md-12">
                                  <input type="text" name="last_name" placeholder="{{Auth::user()->last_name}}" value="{{Auth::user()->last_name}}" class="form-control form-control-line" required>
                              </div>
                            </div>
                            <div class="form-group">
                              <label class="col-md-12">Telefono</label>
                              <div class="col-md-12">
                                  <input type="text" name="phone" placeholder="{{Auth::user()->phone}}" value="{{Auth::user()->phone}}" class="form-control form-control-line" required>
                              </div>
                            </div>
                            <div class="form-group">
                                <label for="example-email" class="col-md-12">Email</label>
                                <div class="col-md-12">
                                    <input type="email" value="{{Auth::user()->email}}" class="form-control form-control-line" name="email" id="example-email" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-12">Contraseña</label>
                                <div class="col-md-12">
                                    <input type="password" name="password" class="form-control form-control-line">
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-12">
                                    <button class="btn btn-success">Cambiar</button>
                                </div>
                            </div>
                        </form>
                    </div>
                  </div>
                  @if (Auth::user()->group_id == 2)
                    <div class="tab-pane" id="negocio" role="tabpanel">
                      <div class="card-body">
                          <form class="form-horizontal form-material" action="{{route('changebiz', ['business' => Auth::user()->business])}}" method="post">
                            {{ csrf_field() }}
                              <div class="form-group">
                                  <label class="col-md-12">Nombre</label>
                                  <div class="col-md-12">
                                      <input type="text" name="name" placeholder="{{Auth::user()->business->name}}" value="{{Auth::user()->business->name}}" class="form-control form-control-line" required>
                                  </div>
                              </div>

                              <div class="form-group">
                                <label class="col-md-12">Domicilio</label>
                                <div class="col-md-12">
                                    <input type="text" name="address" placeholder="{{Auth::user()->business->address}}" value="{{Auth::user()->business->address}}" class="form-control form-control-line" required>
                                </div>
                              </div>

                              <div class="form-group">
                                <label class="col-md-12">Ciudad</label>
                                <div class="col-md-12">
                                    <input type="text" name="city" placeholder="{{Auth::user()->business->city}}" value="{{Auth::user()->business->city}}" class="form-control form-control-line" required>
                                </div>
                              </div>
                              <div class="form-group">
                                <label class="col-md-12">Codigo Postal</label>
                                <div class="col-md-12">
                                    <input type="number" name="cp" placeholder="{{Auth::user()->business->cp}}" value="{{Auth::user()->business->cp}}" class="form-control form-control-line" required>
                                </div>
                              </div>
                              <div class="form-group">
                                <label class="col-md-12">Provincia</label>
                                <div class="col-md-12">
                                    <input type="text" name="province" placeholder="{{Auth::user()->business->province}}" value="{{Auth::user()->business->province}}" class="form-control form-control-line" required>
                                </div>
                              </div>
                              <div class="form-group">
                                <label class="col-md-12">Telefono</label>
                                <div class="col-md-12">
                                    <input type="text" name="phone" placeholder="{{Auth::user()->business->phone}}" value="{{Auth::user()->business->phone}}" class="form-control form-control-line" required>
                                </div>
                              </div>

                              <div class="form-group">
                                  <label class="col-md-12">Email</label>
                                  <div class="col-md-12">
                                      <input type="email" value="{{Auth::user()->business->email}}" class="form-control form-control-line" name="email"  required>
                                  </div>
                              </div>


                              <div class="form-group">
                                  <div class="col-sm-12">
                                      <button class="btn btn-success">Cambiar</button>
                                  </div>
                              </div>
                          </form>
                      </div>
                    </div>
                    {{--
                    @php
                      if (Auth::user()->business->activeSubscription()) {
                        $subscription = Auth::user()->business->activeSubscription();
                      }
                      else {
                        $subscription = Auth::user()->business->lastActiveSubscription();
                      }
                    @endphp
                    <div class="tab-pane" id="plan" role="tabpanel">
                      <div class="card-body">
                        <div class="pricing-plan">
                          <div class="pricing-box featured-plan">
                              <div class="pricing-body">
                                  <div class="pricing-header">
                                      <h4 class="price-lable text-white bg-warning"> Actual</h4>
                                      <h4 class="text-center">{{$subscription->plan->name}}</h4>
                                      <h3 class="text-center"> @if ($subscription->plan->price == 0)
                                        Gratuito
                                      @else <span class="price-sign">$</span> {{$subscription->plan->price}} ({{$subscription->plan->currency}})@endif</h3>
                                      <p class="uppercase">por {{$subscription->plan->duration}} dias.</p>
                                  </div>
                                  <div class="price-table-content">
                                      <div class="price-row"><i class="icon-user"></i> @if ($subscription->features()->code('users.amount')->first()->limit != -1)
                                        {{$subscription->features()->code('users.amount')->first()->limit}} Usuarios
                                      @else
                                        Usuarios Ilimitados
                                      @endif</div>
                                      <div class="price-row"><i class="icon-bag"></i> @if ($subscription->features()->code('products.amount')->first()->limit != -1)
                                        {{$subscription->features()->code('products.amount')->first()->limit}} Productos
                                      @else
                                        Productos Ilimitados
                                      @endif</div>
                                      <div class="price-row"><i class="icon-basket-loaded"></i> @if ($subscription->features()->code('sales.amount')->first()->limit != -1)
                                        {{$subscription->features()->code('sales.amount')->first()->limit}} Ventas
                                      @else
                                        Ventas Ilimitadas
                                      @endif</div>
                                      <div class="price-row"><i class="icon-emotsmile"></i> @if ($subscription->features()->code('clients.amount')->first()->limit != -1)
                                        {{$subscription->features()->code('clients.amount')->first()->limit}} Clientes
                                      @else
                                        Clientes Ilimitados
                                      @endif</div>
                                      <div class="price-row"><i class="icon-briefcase"></i> @if ($subscription->features()->code('entries.amount')->first()->limit != -1)
                                        {{$subscription->features()->code('entries.amount')->first()->limit}} Ordenes
                                      @else
                                        Ordenes Ilimitadas
                                      @endif</div>
                                      <div class="price-row">
                                          <button class="btn btn-lg btn-info waves-effect waves-light m-t-20">Quedan {{$subscription->remainingDays()}} dias</button>
                                      </div>
                                  </div>
                              </div>
                          </div>
                        </div>
                        <center><h2>Otros planes</h2></center>
                        <div class="row pricing-plan">
                                    <div class="col-md-3 col-xs-12 col-sm-6 no-padding">
                                        <div class="pricing-box">
                                            <div class="pricing-body b-l">
                                                <div class="pricing-header">
                                                    <h4 class="text-center">Basico</h4>
                                                    <h2 class="text-center"><span class="price-sign">$</span>8</h2>
                                                    <p class="uppercase">por mes</p>
                                                </div>
                                                <div class="price-table-content">
                                                    <div class="price-row"><i class="icon-user"></i> 3 Usuarios</div>
                                                    <div class="price-row"><i class="icon-bag"></i> 200 Productos</div>
                                                    <div class="price-row"><i class="icon-basket-loaded"></i> 1000 Ventas</div>
                                                    <div class="price-row"><i class="icon-emotsmile"></i> 100 clientes</div>
                                                    <div class="price-row">
                                                        <button class="btn btn-success waves-effect waves-light m-t-20">Contratar</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-xs-12 col-sm-6 no-padding">
                                        <div class="pricing-box b-l">
                                            <div class="pricing-body">
                                                <div class="pricing-header">
                                                    <h4 class="text-center">Plus</h4>
                                                    <h2 class="text-center"><span class="price-sign">$</span>10</h2>
                                                    <p class="uppercase">por mes</p>
                                                </div>
                                                <div class="price-table-content">
                                                    <div class="price-row"><i class="icon-user"></i> 5 Usuarios</div>
                                                    <div class="price-row"><i class="icon-bag"></i> 300 Productos</div>
                                                    <div class="price-row"><i class="icon-basket-loaded"></i> 2000 Ventas</div>
                                                    <div class="price-row"><i class="icon-emotsmile"></i> 200 clientes</div>
                                                    <div class="price-row">
                                                        <button class="btn btn-success waves-effect waves-light m-t-20">Contratar</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-xs-12 col-sm-6 no-padding">
                                        <div class="pricing-box featured-plan">
                                            <div class="pricing-body">
                                                <div class="pricing-header">
                                                    <h4 class="price-lable text-white bg-warning"> Mas vendido</h4>
                                                    <h4 class="text-center">Nitro</h4>
                                                    <h2 class="text-center"><span class="price-sign">$</span>15</h2>
                                                    <p class="uppercase">por mes</p>
                                                </div>
                                                <div class="price-table-content">
                                                    <div class="price-row"><i class="icon-user"></i> 10 Usuarios</div>
                                                    <div class="price-row"><i class="icon-bag"></i> 400 Productos</div>
                                                    <div class="price-row"><i class="icon-basket-loaded"></i> 5000 Ventas</div>
                                                    <div class="price-row"><i class="icon-emotsmile"></i> 400 clientes</div>
                                                    <div class="price-row">
                                                        <button class="btn btn-lg btn-info waves-effect waves-light m-t-20">Contratar</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-xs-12 col-sm-6 no-padding">
                                        <div class="pricing-box">
                                            <div class="pricing-body b-r">
                                                <div class="pricing-header">
                                                    <h4 class="text-center">Ilimitado</h4>
                                                    <h2 class="text-center"><span class="price-sign">$</span>30</h2>
                                                    <p class="uppercase">por mes</p>
                                                </div>
                                                <div class="price-table-content">
                                                  <div class="price-row"><i class="icon-user"></i> Usuarios Ilimitados</div>
                                                  <div class="price-row"><i class="icon-bag"></i> Productos Ilimitados</div>
                                                  <div class="price-row"><i class="icon-basket-loaded"></i> Ventas Ilimitadas</div>
                                                  <div class="price-row"><i class="icon-emotsmile"></i> Clientes Ilimitados</div>
                                                    <div class="price-row">
                                                        <button class="btn btn-success waves-effect waves-light m-t-20">Contratar</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>



                      </div>
                    </div>--}}
                  @endif

              </div>
          </div>
      </div>
      <!-- Column -->
  </div>
@endsection
