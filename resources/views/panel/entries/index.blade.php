@extends('panel.layouts.panel')
@section('content')
<br>
    <div class="card card-outline-info">
            <div class="card-header">
                    <h4 class="m-b-0 text-white text-center">Ordenes de trabajo</h4>
                </div>
        <div class="card-body p-b-0">
            @include('vendor.errors')
            @include('panel.sections.new', [
                'name' => '',
                'redirect' => route('entries.create'),
                'button' => 'Nueva orden'
            ])
            <br>
        </div>
        <!-- Nav tabs -->
        <ul class="nav nav-tabs customtab" role="tablist">
            <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#ordenes" role="tab" aria-expanded="true"><span class="hidden-sm-up"><i class="ti-home"></i></span> <span class="hidden-xs-down">Todo</span></a> </li>
            <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#pendiente" role="tab" aria-expanded="false"><span class="hidden-sm-up"><i class="ti-user"></i></span> <span class="hidden-xs-down">Pendiente</span></a> </li>
            <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#finalizado" role="tab" aria-expanded="false"><span class="hidden-sm-up"><i class="ti-user"></i></span> <span class="hidden-xs-down">Finalizado</span></a> </li>
            <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#entregadocancelado" role="tab" aria-expanded="false"><span class="hidden-sm-up"><i class="ti-email"></i></span> <span class="hidden-xs-down">Entregado/Cancelado</span></a> </li>
        </ul>
        <!-- Tab panes -->
        <div class="tab-content">
            <div class="tab-pane p-20 active" id="ordenes" role="tabpanel" aria-expanded="false">
                <h4 class="card-title text-center">Ordenes</h4>
                <div class="table-responsive">
                    <table id="myTable" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Cliente</th>
                                <th>Tipo</th>
                                <th>Fecha</th>
                                <th>Estado</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($entries as $entry)


                                <tr class='clickable-row' data-href='{{route('entries.show', ['id' => $entry->id])}}'>
                                    <td>{{$entry->id}}</td>
                                    <td>{{$entry->client->name}}</td>
                                    @if ($entry->type == 1)
                                        <td>Presupuesto ({{$entry->marca}} {{$entry->modelo}})</td>
                                    @elseif ($entry->type == 2)
                                        <td>Reparación ({{$entry->marca}} {{$entry->modelo}})</td>
                                    @elseif ($entry->type == 3)
                                        <td>Garantía ({{$entry->marca}} {{$entry->modelo}})</td>
                                    @endif
                                    <td>{{$entry->created_at}}</td>
                                    @if ($entry->status == 0)
                                        <td><span class="label label-info">Pendiente</span> </td>
                                    @elseif ($entry->status == 1)
                                        <td><span class="label label-success">Finalizado</span> </td>
                                    @elseif ($entry->status == 2)
                                        <td><span class="label label-success">Entregado</span> </td>
                                    @elseif ($entry->status == 3)
                                        <td><span class="label label-danger">Cancelado</span> </td>
                                    @endif
                                </tr>
                            @endforeach


                        </tbody>
                    </table>
                </div>
            </div>
            <div class="tab-pane p-20" id="pendiente" role="tabpanel" aria-expanded="false">
                <h4 class="card-title text-center">Ordenes Pendientes</h4>
                <div class="table-responsive">
                    <table id="myTable2" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Cliente</th>
                                <th>Tipo</th>
                                <th>Fecha</th>
                                <th>Estado</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($entries as $entry)
                                @if ($entry->status == 0)
                                    <tr class='clickable-row' data-href='{{route('entries.show', ['id' => $entry->id])}}'>
                                        <td>{{$entry->id}}</td>
                                        <td>{{$entry->client->name}}</td>
                                        @if ($entry->type == 1)
                                            <td>Presupuesto ({{$entry->marca}} {{$entry->modelo}})</td>
                                        @elseif ($entry->type == 2)
                                            <td>Reparación ({{$entry->marca}} {{$entry->modelo}})</td>
                                        @elseif ($entry->type == 3)
                                            <td>Garantía ({{$entry->marca}} {{$entry->modelo}})</td>
                                        @endif
                                        <td>{{$entry->created_at}}</td>
                                        @if ($entry->status == 0)
                                            <td><span class="label label-info">Pendiente</span> </td>
                                        @elseif ($entry->status == 1)
                                            <td><span class="label label-success">Finalizado</span> </td>
                                        @elseif ($entry->status == 2)
                                            <td><span class="label label-success">Entregado</span> </td>
                                        @elseif ($entry->status == 3)
                                            <td><span class="label label-danger">Cancelado</span> </td>
                                        @endif
                                    </tr>
                                @endif

                            @endforeach


                        </tbody>
                    </table>
                </div>
            </div>
            <div class="tab-pane p-20" id="finalizado" role="tabpanel" aria-expanded="false">
                <h4 class="card-title text-center">Ordenes finalizadas</h4>
                <div class="table-responsive">
                    <table id="myTable3" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Cliente</th>
                                <th>Tipo</th>
                                <th>Fecha</th>
                                <th>Estado</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($entries as $entry)
                                @if ($entry->status == 1)


                                    <tr class='clickable-row' data-href='{{route('entries.show', ['id' => $entry->id])}}'>
                                        <td>{{$entry->id}}</td>
                                        <td>{{$entry->client->name}}</td>
                                        @if ($entry->type == 1)
                                            <td>Presupuesto ({{$entry->marca}} {{$entry->modelo}})</td>
                                        @elseif ($entry->type == 2)
                                            <td>Reparación ({{$entry->marca}} {{$entry->modelo}})</td>
                                        @elseif ($entry->type == 3)
                                            <td>Garantía ({{$entry->marca}} {{$entry->modelo}})</td>
                                        @endif
                                        <td>{{$entry->created_at}}</td>
                                        @if ($entry->status == 0)
                                            <td><span class="label label-info">Pendiente</span> </td>
                                        @elseif ($entry->status == 1)
                                            <td><span class="label label-success">Finalizado</span> </td>
                                        @elseif ($entry->status == 2)
                                            <td><span class="label label-success">Entregado</span> </td>
                                        @elseif ($entry->status == 3)
                                            <td><span class="label label-danger">Cancelado</span> </td>
                                        @endif
                                    </tr>
                                @endif

                            @endforeach


                        </tbody>
                    </table>
                </div>
            </div>
            <div class="tab-pane" id="entregadocancelado" role="tabpanel" aria-expanded="true">
                <div class="p-20">
                    <h4 class="card-title text-center">Ordenes Entregadas/Canceladas</h4>
                    <div class="table-responsive">
                        <table id="myTable4" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Cliente</th>
                                    <th>Tipo</th>
                                    <th>Fecha</th>
                                    <th>Estado</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($entries as $entry)
                                    @if ($entry->status == 2 || $entry->status == 3)



                                        <tr class='clickable-row' data-href='{{route('entries.show', ['id' => $entry->id])}}'>
                                            <td>{{$entry->id}}</td>
                                            <td>{{$entry->client->name}}</td>
                                            @if ($entry->type == 1)
                                                <td>Presupuesto ({{$entry->marca}} {{$entry->modelo}})</td>
                                            @elseif ($entry->type == 2)
                                                <td>Reparación ({{$entry->marca}} {{$entry->modelo}})</td>
                                            @elseif ($entry->type == 3)
                                                <td>Garantía ({{$entry->marca}} {{$entry->modelo}})</td>
                                            @endif
                                            <td>{{$entry->created_at}}</td>
                                            @if ($entry->status == 0)
                                                <td><span class="label label-info">Pendiente</span> </td>
                                            @elseif ($entry->status == 1)
                                                <td><span class="label label-success">Finalizado</span> </td>
                                            @elseif ($entry->status == 2)
                                                <td><span class="label label-success">Entregado</span> </td>
                                            @elseif ($entry->status == 3)
                                                <td><span class="label label-danger">Cancelado</span> </td>
                                            @endif
                                        </tr>
                                    @endif
                                @endforeach


                            </tbody>
                        </table>
                    </div>
                </div>
            </div>


        </div>
    </div>

@endsection
@section('scripts')
    <script src="{{asset('assets/plugins/datatables/jquery.dataTables.min.js')}}"></script>
    <!-- start - This is for export functionality only -->
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#myTable').DataTable({
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
                }
            });
        });
        $(document).ready(function() {
            $('#myTable2').DataTable({
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
                }
            });
        });
        $(document).ready(function() {
            $('#myTable3').DataTable({
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
                }
            });
        });
        $(document).ready(function() {
            $('#myTable4').DataTable({
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
                }
            });
        });

    </script>


@endsection
