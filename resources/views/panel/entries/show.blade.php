@extends('panel.layouts.panel')
@section('content')
<br>
  <div class="row" id="printableArea">
      <div class="col-md-12">

          <div class="card card-body printableArea">
              <h3><b>ORDEN DE TRABAJO   @if ($entry->status == 0)
                  <span class="label label-info">PENDIENTE</span>
                @elseif ($entry->status == 1)
                  <span class="label label-success">A ENTREGAR (Finalizado)</span>
                @elseif ($entry->status == 2)
                  <span class="label label-success">ENTREGADA</span>
                @elseif ($entry->status == 3)
                  <span class="label label-danger">CANCELADA</span>
              @endif</b> <span class="pull-right">#{{$entry->id}}</span></h3>
              <hr>
              <div class="row">
                  <div class="col-md-12">
                      <div class="pull-left">
                          <address>
                              <h3> &nbsp;<b class="text-danger">{{Auth::user()->business->name}}</b></h3>
                              <p class="text-muted m-l-5">{{Auth::user()->business->address}}
                                  <br/> {{Auth::user()->business->city}} ({{Auth::user()->business->cp}})
                                  <br/> {{Auth::user()->business->province}}
                                  <br/> Tel.: {{Auth::user()->business->phone}}</p>
                          </address>
                      </div>
                      <div class="pull-right text-right">
                          <address>
                              <h3>Para,</h3>
                              <h4 class="font-bold">{{$entry->client->name}}</h4>
                              <p class="text-muted m-l-30">{{$entry->client->address}}
                                  <br/> Tel.: {{$entry->client->phone}}</p>
                              <p class="m-t-30"><b>Fecha de emisión :</b> <i class="fa fa-calendar"></i> {{$entry->created_at}}</p>
                          </address>
                      </div>
                  </div>
                  <div class="col-md-6">
                      <p>{{$entry->marca}} {{$entry->modelo}} (@if ($entry->type == 1)
                        Presupuesto
                      @elseif ($entry->type == 2)
                        Reparación
                      @elseif ($entry->type == 3)
                        Garantía
                      @endif)</p>
                      <p>Serie: {{$entry->serie}}</p>
                      <hr>
                      <p> <b>Detalles del equipo:</b> {{$entry->notas}} </p>
                      <p> <b>Anotaciones:</b> {{$entry->notes}} </p>
                      <p>Precio: {{$entry->final_price}}</p>
                  </div>

                  <div class="col-md-12">
                      <div class="pull-right m-t-30 text-right">
                          <p>Sub - Total: ${{$entry->final_price}}</p>
                          <p>Pagado : ${{$entry->pagado}} </p>
                          <hr>
                          <h3><b>Total :</b> ${{$entry->final_price-$entry->pagado}}</h3>
                      </div>
                      <div class="clearfix"></div>
                      <hr>
                      <center><p>Si pasado los 30 dias de la presente, el aparato no fuera retirado, quedará en propiedad de este service, entendido que el titular renuncia al mismo de acuerdo a lo estipulado en los Art. 872 y 873 del Código Civil. En cumplimiento de Normas Legales Vigentes, me hago cargo responsable de haber dejado en reparación al aparato descripto.</p></center>
                      <div class="pull-left m-t-10 text-left">
                        <center><br>
                        <hr>
                      <b>--------Firma--------</b></center>
                      </div>

                      <div class="pull-right m-t-10 text-center">
                        <center><br>
                        <hr>
                      <b>--------DNI--------</b></center>
                      </div>
                  </div>



              </div>
          </div>
      </div>
  </div>
  <div class="clearfix"></div>
  <hr>
  <div class="card card-body">
    <div class="text-right">
        <a class="btn btn-danger" href="{{route('entries.edit', ['id' => $entry->id])}}"> Editar </a>
        <button onclick="printDiv('printableArea')" class="btn btn-default btn-outline" type="button"> <span><i class="fa fa-print"></i> Imprimir</span> </button>
    </div>
  </div>

@endsection
