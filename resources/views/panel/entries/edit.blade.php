@extends('panel.layouts.panel')
@section('content')
<br>
  <div class="row">
      <div class="col-lg-12">
          <div class="card card-outline-info">
              <div class="card-header">
                  <h4 class="m-b-0 text-white">Editar Entrada</h4>
              </div>
              <div class="card-body">
                  @include('vendor.errors')
                  <form action="{{route('entries.update', ['id' => $entry->id])}}" method="post">
                    {{ csrf_field() }}
                    {{ method_field('PUT')}}
                      <div class="form-body">

                        <h3 class="card-title">Cliente</h3>
                        <hr>
                        <div class="row">
                            <div class="col-md-6">
                              <center><label class="control-label">Si el cliente ya esta registrado en la base de datos</label>
                              <select class="select2" name="client_id" style="width: 100%" required>
                                  <option value="" disabled selected>Seleccione un cliente</option>
                                  <optgroup label="Clientes disponibles">
                                    @foreach (Auth::user()->business->clients as $clients)
                                      <option value="{{$clients->id}}" @if ($entry->client_id == $clients->id) selected @endif >{{$clients->id}} - {{$clients->name}} ({{$clients->email}})</option>
                                    @endforeach

                                  </optgroup>

                              </select></center>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                  <center><label class="control-label">Si el cliente es nuevo debe registrarlo</label><br></center>
                                  <center><a class="btn btn-rounded btn-success" href="{{route('clients.create')}}"><b style="color:white;">NUEVO CLIENTE</b></a></center>
                                </div>
                              </div>
                          </div>



                          <!--/row-->
                          <h3 class="box-title m-t-40">Equipo</h3>
                          <hr>
                          <div class="row">
                              <div class="col-md-6 ">
                                  <div class="form-group">
                                      <label>Marca</label>
                                      <input type="text" name="marca" class="form-control" value="{{$entry->marca}}" required>
                                  </div>
                              </div>
                              <div class="col-md-6 ">
                                  <div class="form-group">
                                      <label>Modelo</label>
                                      <input type="text" name="modelo" class="form-control" value="{{$entry->modelo}}" required>
                                  </div>
                              </div>
                          </div>
                          <div class="row">
                              <div class="col-md-12">
                                  <div class="form-group">
                                      <label>Serie (IMEI u Otro Identificador)</label>
                                      <input type="text" name="serie" class="form-control" value="{{$entry->serie}}" required>
                                  </div>
                              </div>

                          </div>
                          <div class="row">
                            <div class="col-md-12">
                              <div class="form-group">
                                <label class="control-label">Detalles del equipo</label>
                                <textarea name="notas" rows="8" cols="80" class="form-control" required>{{$entry->notas}}</textarea>
                              </div>
                            </div>
                          </div>
                          <h3 class="box-title m-t-40">Orden</h3>
                          <hr>
                          <div class="row">
                            <div class="col-md-12">
                              <div class="form-group">
                                <label class="control-label">Anotaciones</label>
                                <textarea name="notes" rows="8" cols="80" class="form-control" required>{{$entry->notes}}</textarea>
                              </div>
                            </div>
                          </div>
                          <div class="row">
                              <div class="col-md-6">
                                  <div class="form-group">
                                      <label class="control-label">Tipo de Orden</label>
                                      <select class="form-control custom-select" name='type' data-placeholder="Elija un tipo" tabindex="1" required>
                                        <option value="1" @if ($entry->type == 1) selected @endif>Presupuesto</option>
                                        <option value="2" @if ($entry->type == 2) selected @endif>Reparación</option>
                                        <option value="3" @if ($entry->type == 3) selected @endif>Garantía</option>
                                      </select>
                                  </div>
                              </div>
                              <div class="col-md-6 ">
                                  <div class="form-group">
                                      <label>Estado</label>
                                      <select class="form-control custom-select" name='status' data-placeholder="Elija un estado" tabindex="1" required>
                                        <option value="0" @if ($entry->status == 0) selected @endif>Pendiente</option>
                                        <option value="1" @if ($entry->status == 1) selected @endif>Finalizado</option>
                                        <option value="2" @if ($entry->status == 2) selected @endif>Entregado</option>
                                        <option value="3" @if ($entry->status == 3) selected @endif>Cancelado</option>
                                      </select>
                                  </div>
                              </div>
                          </div>
                          <div class="row">
                              <div class="col-md-6 ">
                                  <div class="form-group">
                                      <label>Precio FINAL</label>
                                      <input type="number" value="{{$entry->final_price}}" name="final_price" class="form-control" required>
                                  </div>
                              </div>
                              <div class="col-md-6 ">
                                  <div class="form-group">
                                      <label>Seña/Pagado</label>
                                      <input type="number" value="{{$entry->pagado}}" name="pagado" class="form-control" required>
                                  </div>
                              </div>
                          </div>
                      </div>
                      <div class="form-actions">
                          <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> Guardar</button>
                          <a href="{{ route('entries.index') }}" class="btn btn-danger" data-dismiss="modal" >Cancelar</a>
                      </div>
                  </form>
              </div>
          </div>
      </div>
  </div>
@endsection
