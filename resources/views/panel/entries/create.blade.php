@extends('panel.layouts.panel')
@section('content')
<br>
  <div class="row">
      <div class="col-lg-12">
          <div class="card card-outline-info">
              <div class="card-header">
                  <h4 class="m-b-0 text-white">Crear nueva Entrada</h4>
              </div>
              <div class="card-body">
                  @include('vendor.errors')
                  <form action="{{route('entries.store')}}" method="post">
                    {{ csrf_field() }}
                      <div class="form-body">

                        <h3 class="card-title">Cliente</h3>
                        <hr>
                        <div class="row">
                            <div class="col-md-6">
                              <center><label class="control-label">Si el cliente ya esta registrado en la base de datos</label>
                              <select class="select2" name="client_id" style="width: 100%" required>
                                  <optgroup label="Clientes disponibles">
                                    @foreach (Auth::user()->business->clients as $clients)
                                      <option value="{{$clients->id}}">{{$clients->id}} - {{$clients->name}} ({{$clients->email}})</option>
                                    @endforeach

                                  </optgroup>

                              </select></center>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                  <center><label class="control-label">Si el cliente es nuevo debe registrarlo</label><br></center>
                                  <center><a class="btn btn-rounded btn-success" href="{{route('clients.create')}}"><b style="color:white;">NUEVO CLIENTE</b></a></center>
                                </div>
                              </div>
                          </div>



                          <!--/row-->
                          <h3 class="box-title m-t-40">Equipo</h3>
                          <hr>
                          <div class="row">
                              <div class="col-md-6 ">
                                  <div class="form-group">
                                      <label>Marca <small>(Ejemplo: Samsung)</small> </label>
                                      <input type="text" name="marca" class="form-control" required>
                                  </div>
                              </div>
                              <div class="col-md-6 ">
                                  <div class="form-group">
                                      <label>Modelo <small>(Ejemplo: S8 Edge)</small> </label>
                                      <input type="text" name="modelo" class="form-control" required>
                                  </div>
                              </div>
                          </div>
                          <div class="row">
                              <div class="col-md-12">
                                  <div class="form-group">
                                      <label>Serie <small>(IMEI u otro Identificador)</small></label>
                                      <input type="text" name="serie" class="form-control" required>
                                  </div>
                              </div>

                          </div>
                          <div class="row">
                            <div class="col-md-12">
                              <div class="form-group">
                                <label class="control-label">Detalles del equipo <small>(Describa las fallas del equipo)</small> </label>
                                <textarea name="notas" rows="2" cols="80" class="form-control" required></textarea>
                              </div>
                            </div>
                          </div>
                          <h3 class="box-title m-t-40">Orden</h3>
                          <hr>
                          <div class="row">
                            <div class="col-md-12">
                              <div class="form-group">
                                <label class="control-label">Notas para el cliente <small>(Estas notas aparecerán en la orden de trabajo)</small> </label>
                                <textarea name="notes" rows="2" cols="80" class="form-control"></textarea>
                              </div>
                            </div>
                          </div>
                          <div class="row">
                              <div class="col-md-6">
                                  <div class="form-group">
                                      <label class="control-label">Tipo de Orden</label>
                                      <select class="form-control custom-select" name='type' data-placeholder="Elija un tipo" tabindex="1" required>
                                          <option value="1">Presupuesto</option>
                                          <option value="2">Reparación</option>
                                          <option value="3">Garantía</option>
                                      </select>
                                  </div>
                              </div>
                              <div class="col-md-6 ">
                                  <div class="form-group">
                                      <label>Estado</label>
                                      <select class="form-control custom-select" name='status' data-placeholder="Elija un estado" tabindex="1" required>
                                          <option value="0">Pendiente</option>
                                          <option value="1">Finalizado</option>
                                          <option value="2">Entregado</option>
                                          <option value="3">Cancelado</option>
                                      </select>
                                  </div>
                              </div>
                          </div>
                          <div class="row">
                              <div class="col-md-6 ">
                                  <div class="form-group">
                                      <label>Precio FINAL</label>
                                      <input type="number" name="final_price" step="0.01" class="form-control" required>
                                  </div>
                              </div>
                              <div class="col-md-6 ">
                                  <div class="form-group">
                                      <label>Seña/Pagado <small>(Si el cliente pago parte del monto final)</small> </label>
                                      <input type="number" name="pagado" step="0.01" class="form-control" value="0" required>
                                  </div>
                              </div>
                          </div>
                      </div>
                      <div class="form-actions">
                          <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> Guardar</button>
                          <a href="{{ route('entries.index') }}" class="btn btn-danger" data-dismiss="modal" >Cancelar</a>
                      </div>
                  </form>
              </div>
          </div>
      </div>
  </div>
@endsection
