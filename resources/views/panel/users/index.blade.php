@extends('panel.layouts.panel')
@section('content')
<br>
{{-- Main row --}}
<div class="row">

  {{-- ------------ User list ------------------ --}}
  <div class="col-sm-12 col-md-8">
    <div class="card card-outline-info">

      <div class="card-header">
          <h4 class="m-b-0 text-white text-center">Lista de usuarios</h4>
      </div>

      <div class="card-body">
        <div class="table-responsive m-t-40">
          <table id="myTable" class="table table-bordered table-hover">
            <thead>
              <tr>
                <th>#</th>
                <th>Nombre</th>
                <th>Apellido</th>
                <th>Email</th>
              </tr>
            </thead>
            <tbody>
              @foreach ($users as $user)


              <tr id="user{{$user->id}}" data-toggle="modal" data-target="#infou{{$user->id}}" data-whatever="@mdo">
                <td>{{$user->id}}</td>
                <td>{{$user->first_name}}</td>
                <td>{{$user->last_name}}</td>
                <td>{{$user->email}}</td>
              </tr>
              {{--------------------- Modal ---------------}}
              <div class="modal fade" id="infou{{$user->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1" style="display: none;" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h4 class="modal-title" id="exampleModalLabel1">{{$user->first_name}} {{$user->last_name}} (#{{$user->id}})</h4>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    </div>
                    <div class="modal-body">
                      <p>Nombre y Apellido: <b>{{$user->first_name}} {{$user->last_name}}</b> </p>
                      <p>Email: {{$user->email}}</p>
                      <p>Teléfono: {{$user->phone}}</p>
                    </div>
                    <div class="modal-footer">
                      <form id="formdelete{{$user->id}}" action="{{route('users.destroy', ['user' => $user])}}" method="post">
                        {{ csrf_field() }}
                        {{ method_field('delete') }}
                      </form>
                      <button type="button" class="btn btn-danger btn-rounded" id="sa-eliminar{{$user->id}}">Borrar</button>
                      <a href="{{route('users.edit', ['user' => $user])}}" class="btn btn-primary btn-rounded">Editar</a>
                      <button type="button" class="btn btn-default btn-rounded" data-dismiss="modal">Aceptar</button>
                    </div>
                  </div>
                </div>
              </div>
              {{--------------------------------}}

              @endforeach
              @if (Auth::user()->business->users->count() == 0)
              <tr>
                <td colspan="4" class="text-center">No se encontraron resultados</td>
              </tr>
              @endif
            </tbody>
          </table>
        </div>
      </div>
      
    </div>
  </div>
  {{-- ----------------- --}}

  {{--------------------- New user --------------}}
  <div class="col-sm-12 col-md-4">
    <div class="card card-outline-info">
      <div class="card-header">
          <h4 class="m-b-0 text-white text-center">Nuevo usuario</h4>
      </div>
      <div class="card-body">
        @include('vendor.errors')
        <form action="{{route('users.store')}}" method="post">
          {{ csrf_field() }}

          <div class="form-group">
            <label class="control-label">Nombre</label>
            <input type="text" name='first_name' id="first_name" class="form-control" placeholder="Nombre " required>
            <small class="form-control-feedback"> Nombre del usuario</small>
          </div>

          <div class="form-group">
            <label class="control-label">Apellido</label>
            <input type="text" name='last_name' id="last_name" class="form-control" placeholder="Apellido" required>
            <small class="form-control-feedback"> Apellido del usuario</small>
          </div>

          <div class="form-group">
            <label class="control-label">Dirección</label>
            <input type="text" name="address" class="form-control" placeholder="Calle Numero, Ciudad(CP)">
            <small class="form-control-feedback"> Direccion del usuario </small>
          </div>

          <div class="form-group">
            <label class="control-label">Teléfono</label>
            <input type="text" name="phone" class="form-control" placeholder="+54 9 11 8899-8899">
            <small class="form-control-feedback"> Numero de telefono del usuario </small>
          </div>

          <div class="form-group">
            <label class="control-label">Email</label>
            <input type="text" name='email' id="email" class="form-control form-control-danger" placeholder="email@usuario.com">
            <small class="form-control-feedback"> Correo electrónico del usuario </small>
          </div>

          <div class="form-group">
            <label class="control-label">Contraseña</label>
            <input type="password" name='password' id="password" class="form-control form-control-danger" placeholder="">
            <small class="form-control-feedback"> La contraseña que utilizará el usuario para ingresar </small>
          </div>

          <div class="form-actions">
            <button type="submit" class="btn btn-block btn-rounded btn-success"> <i class="fa fa-check"></i> Guardar</button>
          </div>
        </form>
      </div>
    </div>
  </div>
  {{-- --------------------------------------- --}}

</div>
{{-- main row --}}

@endsection

@section('scripts')
<script src="{{asset('assets/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<!-- start - This is for export functionality only -->
<script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
<script>
  $(document).ready(function() {

    $('#myTable').DataTable({
      "language": {
        "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
      }
    });
  });
  ! function($) {
    "use strict";

    var SweetAlert = function() {};

    //examples
    SweetAlert.prototype.init = function() {
        @foreach($users as $user)
        $('#sa-eliminar{{$user->id}}').click(function() {
          swal({
            title: "¿Estás seguro?",
            text: "Eliminarás el usuario '{{$user->first_name}} {{$user->last_name}}'",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "¡Borralo!",
            cancelButtonText: "Cancelar",
            closeOnConfirm: false
          }, function(e) {
            if (e) {
              swal("Eliminado", "El usuario fue eliminado.", "success");

              $('#formdelete{{$user->id}}').submit();
              $("#user{{$user->id}}").remove();
            } else {
              swal("Cancelado", "Tu usuario está a salvo.", "success");
            }


          });
        });
        @endforeach





      },
      //init
      $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
  }(window.jQuery),

  //initializing
  function($) {
    "use strict";
    $.SweetAlert.init()
  }(window.jQuery);
</script>
<script src="{{asset('assets/plugins/sweetalert/sweetalert.min.js')}}"></script>
<script src="{{asset('assets/plugins/sweetalert/jquery.sweet-alert.custom.js')}}"></script>

@endsection