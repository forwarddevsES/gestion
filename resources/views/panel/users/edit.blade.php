@extends('panel.layouts.panel')
@section('content')
<br>
  <div class="row">
      <div class="col-lg-12">
          <div class="card card-outline-info">
              <div class="card-header">
                  <h4 class="m-b-0 text-white text-center">Editar usuario</h4>
              </div>
              <div class="card-body">
                  @include('vendor.errors')
                  <form action="{{route('users.update', ['user' => $user])}}" method="post">
                    {{ csrf_field() }}
                    {{ method_field('PUT')}}
                      <div class="form-body">
                          <div class="row p-t-20">
                              <div class="col-md-6">
                                  <div class="form-group">
                                      <label class="control-label">Nombre</label>
                                      <input type="text" name='first_name' id="first_name" class="form-control" placeholder="Nombre" value="{{$user->first_name}}" required>
                                      <small class="form-control-feedback"> Nombre  del usuario</small> </div>
                              </div>
                              <div class="col-md-6">
                                  <div class="form-group">
                                      <label class="control-label">Apellido</label>
                                      <input type="text" name='last_name' id="last_name" class="form-control" placeholder="Apellido" value="{{$user->last_name}}" required>
                                      <small class="form-control-feedback"> Apellido del usuario</small> </div>
                              </div>


                          </div>

                          <div class="row">

                              <div class="col-md-6">
                                  <div class="form-group">
                                      <label class="control-label">Dirección</label>
                                      <input type="text" value="{{$user->address}}" name="address" class="form-control" placeholder="Calle Numero, Ciudad(CP)" required>
                                      <small class="form-control-feedback"> Direccion del usuario </small> </div>
                              </div>

                              <div class="col-md-6">
                                  <div class="form-group">
                                    <label class="control-label">Teléfono</label>
                                    <input type="string" value="{{$user->phone}}" name="phone" class="form-control" placeholder="1166778899" required>
                                    <small class="form-control-feedback"> Numero de telefono del usuario </small> </div>
                                  </div>
                              </div>

                          </div>
                          <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Email</label>
                                    <input type="text" value="{{$user->email}}" name='email'  class="form-control form-control-danger" placeholder="email@usuario.com" >
                                    <small class="form-control-feedback"> Correo electrónico del usuario </small> </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Contraseña</label>
                                    <input type="password" name='newpassword'  class="form-control form-control-danger" placeholder="" >
                                    <small class="form-control-feedback"> La contraseña que utilizará el usuario para ingresar </small> </div>
                            </div>
                          </div>


                      <div class="form-actions">
                          <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> Guardar</button>
                          <a href="{{ route('users.index') }}" class="btn btn-danger" data-dismiss="modal" >Cancelar</a>
                      </div>
                  </form>
              </div>
          </div>
      </div>
  </div>
@endsection
