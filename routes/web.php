<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('panel');
});

Route::get('/prueba', function () {
    $user = Auth::user()->with('business')->with('business.entries')->first();
    return dd($user);
})->name('prueba');
Route::post('buscar', 'HomeController@buscar')->name('buscar');
Auth::routes(['register' => false]);

Route::prefix('panel')->group(function () {
    Route::get('/', 'HomeController@index')->name('panel');
    Route::get('account', 'HomeController@account')->name('settings');
    Route::post('changeuser/{user}', 'HomeController@changeuser')->name('changeuser');
    Route::post('changebiz/{business}', 'HomeController@changebiz')->name('changebiz');
    Route::resource('users', 'UserController');
    Route::resource('sales', 'SalesController');
    Route::resource('clients', 'ClientController');
    Route::resource('entries', 'EntryController');
    Route::resource('products', 'ProductsController');
    Route::resource('providers', 'ProvidersController');
    Route::resource('items', 'ProvidersItemController');
});

Route::prefix('admin')->group(function () {
    Route::get('/', 'AdminController@index')->name('admin.index');
    Route::get('users', 'AdminController@userIndex')->name('admin.users.index');

});
