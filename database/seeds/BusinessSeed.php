<?php

use Illuminate\Database\Seeder;
/*use Rennokki\Plans\Traits\HasPlans;
use Rennokki\Plans\Models\PlanModel;*/
use App\Business;
use App\Client;
class BusinessSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      //$plan = PlanModel::where('name', 'Prueba')->first();
      $business = Business::create([
        'name' => 'Sin Definir',
        'address' => 'Sin Definir',
        'email' => 'sin@definir.com',
        'city' => 'Sin Definir',
        'cp' => '1663',
        'province' => 'Prov. de Buenos Aires',
        'phone' => '11-1111-1111',
      ]);
      //$business->subscribeTo($plan, 15);
      Client::create([
        'business_id' => $business->id,
        'name' => 'CONSUMIDOR FINAL'
      ]);
    }
}
