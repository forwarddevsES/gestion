<?php

use Illuminate\Database\Seeder;

use App\User;
class UserSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $user1 = User::create([
        'first_name' => 'Admin',
        'last_name' => 'Admin',
        'email' => 'admin@admin.com',
        'password' => bcrypt('admin'),
        'business_id' => '1',
        'group_id' => '2',
      ]);
    }
}
