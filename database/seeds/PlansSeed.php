<?php

use Illuminate\Database\Seeder;
/*use Rennokki\Plans\Models\PlanModel;
use Rennokki\Plans\Traits\HasPlans;
use Rennokki\Plans\Models\PlanFeatureModel;*/

class PlansSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      /*$basico = PlanModel::create([
          'name' => 'Basico',
          'description' => 'Plan basico',
          'price' => 8.00,
          'currency' => 'USD',
          'duration' => 30, // in days
      ]);
      $basico->features()->saveMany([
          new PlanFeatureModel([
              'name' => 'Acceso a usuarios',
              'code' => 'users.access',
              'description' => 'Le da acceso al modulo de usuarios.',
              'type' => 'feature',
          ]),
          new PlanFeatureModel([
              'name' => 'Limite de Usuarios',
              'code' => 'users.amount',
              'description' => 'La cantidad máxima de usuarios que puede registrar',
              'type' => 'limit',
              'limit' => 3, // or any negative value
          ]),
          new PlanFeatureModel([
              'name' => 'Acceso a ordenes',
              'code' => 'entries.access',
              'description' => 'Le da acceso al modulo de ordenes.',
              'type' => 'feature',
          ]),
          new PlanFeatureModel([
              'name' => 'Limite de Ordenes',
              'code' => 'entries.amount',
              'description' => 'La cantidad máxima de ordenes de trabajo que puede registrar',
              'type' => 'limit',
              'limit' => 500, // or any negative value
          ]),
          new PlanFeatureModel([
              'name' => 'Acceso a ventas',
              'code' => 'sales.access',
              'description' => 'Le da acceso al modulo de ventas.',
              'type' => 'feature',
          ]),
          new PlanFeatureModel([
              'name' => 'Limite de Ventas',
              'code' => 'sales.amount',
              'description' => 'La cantidad máxima de ventas que puede registrar',
              'type' => 'limit',
              'limit' => 1000, // or any negative value
          ]),
          new PlanFeatureModel([
              'name' => 'Acceso a productos',
              'code' => 'products.access',
              'description' => 'Le da acceso al modulo de productos.',
              'type' => 'feature',
          ]),
          new PlanFeatureModel([
              'name' => 'Limite de productos',
              'code' => 'products.amount',
              'description' => 'La cantidad máxima de productos que puede registrar',
              'type' => 'limit',
              'limit' => 200, // or any negative value
          ]),
          new PlanFeatureModel([
              'name' => 'Acceso a compras',
              'code' => 'purchases.access',
              'description' => 'Le da acceso al modulo de compras.',
              'type' => 'feature',
          ]),
          new PlanFeatureModel([
              'name' => 'Limite de compras',
              'code' => 'purchases.amount',
              'description' => 'La cantidad máxima de compras que puede registrar',
              'type' => 'limit',
              'limit' => 1000, // or any negative value
          ]),
          new PlanFeatureModel([
              'name' => 'Acceso a proveedores',
              'code' => 'providers.access',
              'description' => 'Le da acceso al modulo de proveedores.',
              'type' => 'feature',
          ]),
          new PlanFeatureModel([
              'name' => 'Limite de proveedores',
              'code' => 'providers.amount',
              'description' => 'La cantidad máxima de proveedores que puede registrar',
              'type' => 'limit',
              'limit' => 100, // or any negative value
          ]),
          new PlanFeatureModel([
              'name' => 'Acceso a clientes',
              'code' => 'clients.access',
              'description' => 'Le da acceso al modulo de clientes.',
              'type' => 'feature',
          ]),
          new PlanFeatureModel([
              'name' => 'Limite de clientes',
              'code' => 'clients.amount',
              'description' => 'La cantidad máxima de clientes que puede registrar',
              'type' => 'limit',
              'limit' => 100, // or any negative value
          ]),
      ]);

      $plus = PlanModel::create([
          'name' => 'Plus',
          'description' => 'Plan plus',
          'price' => 10.00,
          'currency' => 'USD',
          'duration' => 30, // in days
      ]);
      $plus->features()->saveMany([
          new PlanFeatureModel([
              'name' => 'Acceso a usuarios',
              'code' => 'users.access',
              'description' => 'Le da acceso al modulo de usuarios.',
              'type' => 'feature',
          ]),
          new PlanFeatureModel([
              'name' => 'Limite de Usuarios',
              'code' => 'users.amount',
              'description' => 'La cantidad máxima de usuarios que puede registrar',
              'type' => 'limit',
              'limit' => 5, // or any negative value
          ]),
          new PlanFeatureModel([
              'name' => 'Acceso a ordenes',
              'code' => 'entries.access',
              'description' => 'Le da acceso al modulo de ordenes.',
              'type' => 'feature',
          ]),
          new PlanFeatureModel([
              'name' => 'Limite de Ordenes',
              'code' => 'entries.amount',
              'description' => 'La cantidad máxima de ordenes de trabajo que puede registrar',
              'type' => 'limit',
              'limit' => 1000, // or any negative value
          ]),
          new PlanFeatureModel([
              'name' => 'Acceso a ventas',
              'code' => 'sales.access',
              'description' => 'Le da acceso al modulo de ventas.',
              'type' => 'feature',
          ]),
          new PlanFeatureModel([
              'name' => 'Limite de Ventas',
              'code' => 'sales.amount',
              'description' => 'La cantidad máxima de ventas que puede registrar',
              'type' => 'limit',
              'limit' => 2000, // or any negative value
          ]),
          new PlanFeatureModel([
              'name' => 'Acceso a productos',
              'code' => 'products.access',
              'description' => 'Le da acceso al modulo de productos.',
              'type' => 'feature',
          ]),
          new PlanFeatureModel([
              'name' => 'Limite de productos',
              'code' => 'products.amount',
              'description' => 'La cantidad máxima de productos que puede registrar',
              'type' => 'limit',
              'limit' => 300, // or any negative value
          ]),
          new PlanFeatureModel([
              'name' => 'Acceso a compras',
              'code' => 'purchases.access',
              'description' => 'Le da acceso al modulo de compras.',
              'type' => 'feature',
          ]),
          new PlanFeatureModel([
              'name' => 'Limite de compras',
              'code' => 'purchases.amount',
              'description' => 'La cantidad máxima de compras que puede registrar',
              'type' => 'limit',
              'limit' => 2000, // or any negative value
          ]),
          new PlanFeatureModel([
              'name' => 'Acceso a proveedores',
              'code' => 'providers.access',
              'description' => 'Le da acceso al modulo de proveedores.',
              'type' => 'feature',
          ]),
          new PlanFeatureModel([
              'name' => 'Limite de proveedores',
              'code' => 'providers.amount',
              'description' => 'La cantidad máxima de proveedores que puede registrar',
              'type' => 'limit',
              'limit' => 200, // or any negative value
          ]),
          new PlanFeatureModel([
              'name' => 'Acceso a clientes',
              'code' => 'clients.access',
              'description' => 'Le da acceso al modulo de clientes.',
              'type' => 'feature',
          ]),
          new PlanFeatureModel([
              'name' => 'Limite de clientes',
              'code' => 'clients.amount',
              'description' => 'La cantidad máxima de clientes que puede registrar',
              'type' => 'limit',
              'limit' => 200, // or any negative value
          ]),
      ]);
      $nitro = PlanModel::create([
          'name' => 'Nitro',
          'description' => 'Plan nitro',
          'price' => 15.00,
          'currency' => 'USD',
          'duration' => 30, // in days
      ]);
      $nitro->features()->saveMany([
          new PlanFeatureModel([
              'name' => 'Acceso a usuarios',
              'code' => 'users.access',
              'description' => 'Le da acceso al modulo de usuarios.',
              'type' => 'feature',
          ]),
          new PlanFeatureModel([
              'name' => 'Limite de Usuarios',
              'code' => 'users.amount',
              'description' => 'La cantidad máxima de usuarios que puede registrar',
              'type' => 'limit',
              'limit' => 15, // or any negative value
          ]),
          new PlanFeatureModel([
              'name' => 'Acceso a ordenes',
              'code' => 'entries.access',
              'description' => 'Le da acceso al modulo de ordenes.',
              'type' => 'feature',
          ]),
          new PlanFeatureModel([
              'name' => 'Limite de Ordenes',
              'code' => 'entries.amount',
              'description' => 'La cantidad máxima de ordenes de trabajo que puede registrar',
              'type' => 'limit',
              'limit' => 5000, // or any negative value
          ]),
          new PlanFeatureModel([
              'name' => 'Acceso a ventas',
              'code' => 'sales.access',
              'description' => 'Le da acceso al modulo de ventas.',
              'type' => 'feature',
          ]),
          new PlanFeatureModel([
              'name' => 'Limite de Ventas',
              'code' => 'sales.amount',
              'description' => 'La cantidad máxima de ventas que puede registrar',
              'type' => 'limit',
              'limit' => 5000, // or any negative value
          ]),
          new PlanFeatureModel([
              'name' => 'Acceso a productos',
              'code' => 'products.access',
              'description' => 'Le da acceso al modulo de productos.',
              'type' => 'feature',
          ]),
          new PlanFeatureModel([
              'name' => 'Limite de productos',
              'code' => 'products.amount',
              'description' => 'La cantidad máxima de productos que puede registrar',
              'type' => 'limit',
              'limit' => 500, // or any negative value
          ]),
          new PlanFeatureModel([
              'name' => 'Acceso a compras',
              'code' => 'purchases.access',
              'description' => 'Le da acceso al modulo de compras.',
              'type' => 'feature',
          ]),
          new PlanFeatureModel([
              'name' => 'Limite de compras',
              'code' => 'purchases.amount',
              'description' => 'La cantidad máxima de compras que puede registrar',
              'type' => 'limit',
              'limit' => 5000, // or any negative value
          ]),
          new PlanFeatureModel([
              'name' => 'Acceso a proveedores',
              'code' => 'providers.access',
              'description' => 'Le da acceso al modulo de proveedores.',
              'type' => 'feature',
          ]),
          new PlanFeatureModel([
              'name' => 'Limite de proveedores',
              'code' => 'providers.amount',
              'description' => 'La cantidad máxima de proveedores que puede registrar',
              'type' => 'limit',
              'limit' => 400, // or any negative value
          ]),
          new PlanFeatureModel([
              'name' => 'Acceso a clientes',
              'code' => 'clients.access',
              'description' => 'Le da acceso al modulo de clientes.',
              'type' => 'feature',
          ]),
          new PlanFeatureModel([
              'name' => 'Limite de clientes',
              'code' => 'clients.amount',
              'description' => 'La cantidad máxima de clientes que puede registrar',
              'type' => 'limit',
              'limit' => 400, // or any negative value
          ]),
      ]);
      $ilimitado = PlanModel::create([
          'name' => 'Ilimitado',
          'description' => 'Plan Ilimitado',
          'price' => 30.00,
          'currency' => 'USD',
          'duration' => 30, // in days
      ]);
      $ilimitado->features()->saveMany([
          new PlanFeatureModel([
              'name' => 'Acceso a usuarios',
              'code' => 'users.access',
              'description' => 'Le da acceso al modulo de usuarios.',
              'type' => 'feature',
          ]),
          new PlanFeatureModel([
              'name' => 'Limite de Usuarios',
              'code' => 'users.amount',
              'description' => 'La cantidad máxima de usuarios que puede registrar',
              'type' => 'limit',
              'limit' => -1, // or any negative value
          ]),
          new PlanFeatureModel([
              'name' => 'Acceso a ordenes',
              'code' => 'entries.access',
              'description' => 'Le da acceso al modulo de ordenes.',
              'type' => 'feature',
          ]),
          new PlanFeatureModel([
              'name' => 'Limite de Ordenes',
              'code' => 'entries.amount',
              'description' => 'La cantidad máxima de ordenes de trabajo que puede registrar',
              'type' => 'limit',
              'limit' => -1, // or any negative value
          ]),
          new PlanFeatureModel([
              'name' => 'Acceso a ventas',
              'code' => 'sales.access',
              'description' => 'Le da acceso al modulo de ventas.',
              'type' => 'feature',
          ]),
          new PlanFeatureModel([
              'name' => 'Limite de Ventas',
              'code' => 'sales.amount',
              'description' => 'La cantidad máxima de ventas que puede registrar',
              'type' => 'limit',
              'limit' => -1, // or any negative value
          ]),
          new PlanFeatureModel([
              'name' => 'Acceso a productos',
              'code' => 'products.access',
              'description' => 'Le da acceso al modulo de productos.',
              'type' => 'feature',
          ]),
          new PlanFeatureModel([
              'name' => 'Limite de productos',
              'code' => 'products.amount',
              'description' => 'La cantidad máxima de productos que puede registrar',
              'type' => 'limit',
              'limit' => -1, // or any negative value
          ]),
          new PlanFeatureModel([
              'name' => 'Acceso a compras',
              'code' => 'purchases.access',
              'description' => 'Le da acceso al modulo de compras.',
              'type' => 'feature',
          ]),
          new PlanFeatureModel([
              'name' => 'Limite de compras',
              'code' => 'purchases.amount',
              'description' => 'La cantidad máxima de compras que puede registrar',
              'type' => 'limit',
              'limit' => -1, // or any negative value
          ]),
          new PlanFeatureModel([
              'name' => 'Acceso a proveedores',
              'code' => 'providers.access',
              'description' => 'Le da acceso al modulo de proveedores.',
              'type' => 'feature',
          ]),
          new PlanFeatureModel([
              'name' => 'Limite de proveedores',
              'code' => 'providers.amount',
              'description' => 'La cantidad máxima de proveedores que puede registrar',
              'type' => 'limit',
              'limit' => -1, // or any negative value
          ]),
          new PlanFeatureModel([
              'name' => 'Acceso a clientes',
              'code' => 'clients.access',
              'description' => 'Le da acceso al modulo de clientes.',
              'type' => 'feature',
          ]),
          new PlanFeatureModel([
              'name' => 'Limite de clientes',
              'code' => 'clients.amount',
              'description' => 'La cantidad máxima de clientes que puede registrar',
              'type' => 'limit',
              'limit' => -1, // or any negative value
          ]),
      ]);
      $planfree = PlanModel::create([
          'name' => 'Prueba',
          'description' => 'Plan de prueba',
          'price' => 0.00,
          'currency' => 'USD',
          'duration' => 15, // in days
      ]);
      $planfree->features()->saveMany([
          new PlanFeatureModel([
              'name' => 'Acceso a usuarios',
              'code' => 'users.access',
              'description' => 'Le da acceso al modulo de usuarios.',
              'type' => 'feature',
          ]),
          new PlanFeatureModel([
              'name' => 'Limite de Usuarios',
              'code' => 'users.amount',
              'description' => 'La cantidad máxima de usuarios que puede registrar',
              'type' => 'limit',
              'limit' => 3, // or any negative value
          ]),
          new PlanFeatureModel([
              'name' => 'Acceso a ordenes',
              'code' => 'entries.access',
              'description' => 'Le da acceso al modulo de ordenes.',
              'type' => 'feature',
          ]),
          new PlanFeatureModel([
              'name' => 'Limite de Ordenes',
              'code' => 'entries.amount',
              'description' => 'La cantidad máxima de ordenes de trabajo que puede registrar',
              'type' => 'limit',
              'limit' => 500, // or any negative value
          ]),
          new PlanFeatureModel([
              'name' => 'Acceso a ventas',
              'code' => 'sales.access',
              'description' => 'Le da acceso al modulo de ventas.',
              'type' => 'feature',
          ]),
          new PlanFeatureModel([
              'name' => 'Limite de Ventas',
              'code' => 'sales.amount',
              'description' => 'La cantidad máxima de ventas que puede registrar',
              'type' => 'limit',
              'limit' => 1000, // or any negative value
          ]),
          new PlanFeatureModel([
              'name' => 'Acceso a productos',
              'code' => 'products.access',
              'description' => 'Le da acceso al modulo de productos.',
              'type' => 'feature',
          ]),
          new PlanFeatureModel([
              'name' => 'Limite de productos',
              'code' => 'products.amount',
              'description' => 'La cantidad máxima de productos que puede registrar',
              'type' => 'limit',
              'limit' => 200, // or any negative value
          ]),
          new PlanFeatureModel([
              'name' => 'Acceso a compras',
              'code' => 'purchases.access',
              'description' => 'Le da acceso al modulo de compras.',
              'type' => 'feature',
          ]),
          new PlanFeatureModel([
              'name' => 'Limite de compras',
              'code' => 'purchases.amount',
              'description' => 'La cantidad máxima de compras que puede registrar',
              'type' => 'limit',
              'limit' => 1000, // or any negative value
          ]),
          new PlanFeatureModel([
              'name' => 'Acceso a proveedores',
              'code' => 'providers.access',
              'description' => 'Le da acceso al modulo de proveedores.',
              'type' => 'feature',
          ]),
          new PlanFeatureModel([
              'name' => 'Limite de proveedores',
              'code' => 'providers.amount',
              'description' => 'La cantidad máxima de proveedores que puede registrar',
              'type' => 'limit',
              'limit' => 100, // or any negative value
          ]),
          new PlanFeatureModel([
              'name' => 'Acceso a clientes',
              'code' => 'clients.access',
              'description' => 'Le da acceso al modulo de clientes.',
              'type' => 'feature',
          ]),
          new PlanFeatureModel([
              'name' => 'Limite de clientes',
              'code' => 'clients.amount',
              'description' => 'La cantidad máxima de clientes que puede registrar',
              'type' => 'limit',
              'limit' => 100, // or any negative value
          ]),
      ]);*/
    }
}
