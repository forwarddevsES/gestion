<?php

use Faker\Generator as Faker;

$factory->define(App\Ajuste::class, function (Faker $faker) {
    return [
      'clave' => $faker->word(),
      'valor' => $faker->word()
    ];
});
