<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EntryStoreRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        /*$subscription = auth()->user()->business->activeSubscription();
        if(auth()->user()->business->entries->count() < $subscription->features()->code('entries.amount')->first()->limit)
        {
            return true;
        }
        return false;*/
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'client_id' => 'required|integer|exists:clients,id',
            'type' => 'required|integer',
            'notes' => 'string|alpha_dash|max:255',
            'status' => 'required|integer',
            'pagado' => 'required|numeric',
            'final_price' => 'required|numeric|gte:pagado',
            'marca' => 'required|alpha_dash|max:128',
            'modelo' => 'required|alpha_dash|max:128',
            'serie' => 'required|alpha_dash|max:25',
            'notas' => 'required|alpha_dash|max:255',
        ];
    }
}
