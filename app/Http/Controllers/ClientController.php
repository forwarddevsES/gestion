<?php

namespace App\Http\Controllers;

use Requestt;
use App\Client;
use Auth;
use Session;
use Redirect;
use Illuminate\Http\Request;

class ClientController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        Session::flash('url',Requestt::server('HTTP_REFERER'));
        return view('panel.clients.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        /*$subscription = Auth::user()->business->activeSubscription();
        if (Auth::user()->business->clients->count() < $subscription->features()->code('clients.amount')->first()->limit) {
            Session::flash('url',Requestt::server('HTTP_REFERER'));
            return view('panel.clients.create');
        }
        else {
            return redirect()->back()->with('warning', 'Alcanzaste el limite de clientes en tu plan.');
        }*/
        Session::flash('url',Requestt::server('HTTP_REFERER'));
        return view('panel.clients.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->merge(['business_id' => Auth::user()->business->id]);
        $validatedData = $request->validate([
            'business_id' => 'required|integer|exists:businesses,id',
            'name' => 'required|string',
            'email' => 'required|string|email|max:255|unique:clients',
            'address' => 'required|string',
            'phone' => 'required|string'
        ]);
        Client::create($request->all());
        /* return Redirect::to(Session::get('url')); */
        return redirect('panel/clients');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $client = Auth::user()->business->clients->find($id);
        return view('panel.clients.show', compact('client'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $client = Auth::user()->business->clients->find($id);
      return view('panel.clients.edit', compact('client'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->merge(['business_id' => Auth::user()->business->id]);
        $validatedData = $request->validate([
            'business_id' => 'required|integer|exists:businesses,id',
            'name' => 'required|string',
            'email' => 'required|string|email|max:255|unique:clients',
            'address' => 'required|string',
            'phone' => 'required|string'
        ]);
        $client = Client::find($id);
        $client->update($request->all());
        return redirect()->route('clients.index')->with('message', 'Editaste un cliente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $client = Client::find($id);
        if ($client->sales->count()) {
            foreach ($client->sales as $sale) {
                $sale->delete();
            }
        }
        if ($client->entries->count()) {
            foreach ($client->entries as $entry) {
                $entry->delete();
            }
        }
        $client->delete();
        return back()->with('message', 'Eliminaste un cliente');
    }
}
