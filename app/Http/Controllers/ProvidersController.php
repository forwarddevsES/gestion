<?php

namespace App\Http\Controllers;

use Requestt;
use Auth;
use App\Provider;
use App\ProviderItem;
use Session;
use Illuminate\Http\Request;

class ProvidersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function __construct()
    {
        $this->middleware(['auth']);
    }

    public function index()
    {
        Session::flash('url',Requestt::server('HTTP_REFERER'));
        $providers = Auth::user()->business->providers;
        return view('panel.providers.index', compact('providers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        /*$subscription = Auth::user()->business->activeSubscription();
        if (Auth::user()->business->providers->count() < $subscription->features()->code('providers.amount')->first()->limit) {
            Session::flash('url',Requestt::server('HTTP_REFERER'));
            return view('panel.providers.create');
        }
        else {
            return redirect()->back()->with('warning', 'Alcanzaste el limite de proveedores en tu plan.');
        }*/

        Session::flash('url',Requestt::server('HTTP_REFERER'));
        return view('panel.providers.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required',
            'address' => 'required',
            'phone' => 'required',
            'email' => 'required',
            'info' => 'required',
        ]);
        $provider = new Provider();
        $provider->business_id = Auth::user()->business->id;
        $provider->name = $request->input('name');
        $provider->address = $request->input('address');
        $provider->phone = $request->input('phone');
        $provider->email = $request->input('email');
        $provider->info = $request->input('info');
        $provider->save();

        return redirect()->route('providers.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        Session::flash('url',Requestt::server('HTTP_REFERER'));
        $provider = Provider::find($id);
        return view('panel.providers.show', compact('provider'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $provider = Auth::user()->business->providers->find($id);
        return view('panel.providers.edit', compact('provider'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'name' => 'required',
            'address' => 'required',
            'phone' => 'required',
            'email' => 'required',
            'info' => 'required',
        ]);
        $provider = Provider::findOrFail($id);
        $provider->update($request->all());
        return redirect()->route('providers.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $provider = Provider::find($id);
        $provider->delete();
        return redirect()->route('providers.index');
    }
}
