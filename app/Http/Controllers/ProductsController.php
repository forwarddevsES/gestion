<?php

namespace App\Http\Controllers;

use Requestt;
use App\Product;
use Auth;
use Session;
use Illuminate\Http\Request;
class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function __construct()
     {
         $this->middleware(['auth']);
     }
    public function index()
    {
        Session::flash('url',Requestt::server('HTTP_REFERER'));
        $products = Product::orderBy('id', 'desc')->get();
        return view('panel.products.index', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        /*$subscription = Auth::user()->business->activeSubscription();
        if (Auth::user()->business->products->count() < $subscription->features()->code('products.amount')->first()->limit) {
            Session::flash('url',Requestt::server('HTTP_REFERER'));
            return view('panel.products.create');
        }
        else {
            return redirect()->back()->with('warning', 'Alcanzaste el limite de productos en tu plan.');
        }*/

        Session::flash('url',Requestt::server('HTTP_REFERER'));
        return view('panel.products.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'code' => 'required',
            'name' => 'required',
            'amount' => 'required|integer',
            'price' => 'required',
        ]);
        $product = new Product();
        $product->business_id = Auth::user()->business_id;
        $product->code = $request->code;
        $product->name = $request->name;
        $product->amount = $request->amount;
        $product->price = $request->price;
        if ($product->save()) {
          return redirect()->route('products.index')->with('message', 'Se ha creado un nuevo producto.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        Session::flash('url',Requestt::server('HTTP_REFERER'));
        $product = Product::where('id', $id)->first();
        return view('panel.products.show', compact('product'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      Session::flash('url',Requestt::server('HTTP_REFERER'));
      $product = Product::where('id', $id)->first();
      return view('panel.products.edit', compact('product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'code' => 'required',
            'name' => 'required',
            'amount' => 'required|integer',
            'price' => 'required',
        ]);
      $product = Product::find($id);
      $product->code = $request->code;
      $product->name = $request->name;
      $product->amount = $request->amount;
      $product->price = $request->price;
      if ($product->save()) {
        return back()->with('message', 'Editaste el producto correctamente.');
      }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::find($id);
        $product->delete();
        return back();
    }
}
