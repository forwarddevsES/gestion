<?php

namespace App\Http\Controllers;

use Requestt;
use Auth;
use App\Sale;
use App\SaleItem;
use Session;
use App\Product;
use Illuminate\Http\Request;

class SalesController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        Session::flash('url',Requestt::server('HTTP_REFERER'));
        $sales = Auth::user()->business->sales;
        return view('panel.sales.index', compact('sales'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        /*$subscription = Auth::user()->business->activeSubscription();
        if (Auth::user()->business->sales->count() < $subscription->features()->code('sales.amount')->first()->limit) {
            Session::flash('url',Requestt::server('HTTP_REFERER'));
            return view('panel.sales.create');
        }
        else {
            return redirect()->back()->with('warning', 'Alcanzaste el limite de ventas en tu plan.');
        }
        */
        Session::flash('url',Requestt::server('HTTP_REFERER'));
        return view('panel.sales.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'client_id' => 'required|integer|exists:clients,id',
            'products' => 'required',
        ]);

        $sale = new Sale();
        $sale->business_id = Auth::user()->business->id;
        $sale->client_id = $request->client_id;
        $sale->final_price = '0';
        $totalprice = 0;
        foreach ($request->products as $product) {
          $productt = Product::find($product['product']);
          if ($product['amount'] >= 1 && $product['amount'] <= $productt->amount) {
            $sale->save();
            $saleitem = new SaleItem();
            $saleitem->sale_id = $sale->id;
            $saleitem->product_id = $productt->id;
            $saleitem->name = $productt->name;
            $saleitem->code = $productt->code;
            $saleitem->price = $productt->price;
            $saleitem->amount = $product['amount'];
            $saleitem->save();

            $productt->amount = $productt->amount-$product['amount'];
            $productt->save();
            $totalprice = $totalprice+($saleitem->product->price*$saleitem->amount);
          }
          else {
            return back()->with('warning', 'Error: No queda stock de '.$productt->name);
          }
        }
        $sale->final_price = $totalprice;
        $sale->save();
        return redirect()->route('sales.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        Session::flash('url',Requestt::server('HTTP_REFERER'));
        $sale = Sale::find($id);
        return view('panel.sales.show', compact('sale'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $sale = Sale::find($id);
      foreach ($sale->items as $item) {
          $item->product->amount = $item->product->amount+$item->amount;
          $item->product->save();
          $item->delete();
      }
      $sale->delete();
      return back()->with('message', 'Eliminaste una venta');
    }
}
