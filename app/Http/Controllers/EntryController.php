<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Entry;
use App\Http\Requests\EntryStoreRequest;
use Auth;

class EntryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
         $this->middleware(['auth']);
    }

    public function index()
    {
        /*$subscription = Auth::user()->business->activeSubscription();
        if (Auth::user()->business->entries->count() < $subscription->features()->code('entries.amount')->first()->limit) {
            $entries = Auth::user()->business->entries()->orderBy('id', 'desc')->get();
            return view('panel.entries.index', compact('entries'));
        }
        else {
            return redirect()->back()->with('warning', 'Alcanzaste el limite de ordenes en tu plan.');
        }*/
        $entries = Auth::user()->business->entries()->orderBy('id', 'desc')->get();
        return view('panel.entries.index', compact('entries'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('panel.entries.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EntryStoreRequest $request)
    {
        $request->merge(['business_id' => auth()->user()->business->id]);
        Entry::create($request->all());
        return redirect('panel/entries')->with('message', 'Se creó una nueva entrada');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $entry = Auth::user()->business->entries->find($id);
        return view('panel.entries.show', compact('entry'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $entry = Auth::user()->business->entries->find($id);
        return view('panel.entries.edit', compact('entry', 'item'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(EntryStoreRequest $request, $id)
    {
        $request->merge(['business_id' => auth()->user()->business->id]);
        $entry = Auth::user()->business->entries->find($id);
        $entry->update($request->all());

        return redirect('panel')->with('message', 'Se editó una nueva entrada');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
