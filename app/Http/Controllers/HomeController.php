<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Entry;
use App\Business;
use App\User;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $entries = Entry::orderBy('id', 'desc')->get();
        return view('panel.index', compact('entries'));
    }

    public function account()
    {
        return view('panel.account.settings');
    }
    public function changebiz(Request $request, Business $business)
    {
      $business->name = $request->name;
      $business->address = $request->address;
      $business->city = $request->city;
      $business->cp = $request->cp;
      $business->province = $request->province;
      $business->phone = $request->phone;
      $business->email = $request->email;
      $business->save();
      return back()->with('message', 'Se realizaron ajustes en la información del negocio');
    }
    public function changeuser(Request $request, User $user)
    {
      $user->first_name = $request->first_name;
      $user->last_name = $request->last_name;
      $user->phone = $request->phone;
      //$user->email = $request->email;
      if ($request->password != null) {
        //$user->password = bcrypt($request->password);
      }
      $user->save();
      return back()->with('message', 'Se realizaron los ajustes de tu cuenta');
    }
    public function buscar(Request $request)
    {
      $existe = Entry::where('id', $request->q)->count();
      if ($existe) {
        $result = Entry::where('id', $request->q)->first();
        return redirect()->route('entries.show', ['id' => $result->id]);
      }
      else {
        return back();
      }
    }
}
