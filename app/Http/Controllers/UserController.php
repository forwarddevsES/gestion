<?php

namespace App\Http\Controllers;

use App\User;
use Auth;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'owner']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = Auth::user()->business->users->where('id', '!=', Auth::user()->id);
        return view('panel.users.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $subscription = Auth::user()->business->activeSubscription();
        if (Auth::user()->business->users->count() < $subscription->features()->code('users.amount')->first()->limit) {
            return view('panel.users.create');
        }
        else {
            return redirect()->back()->with('warning', 'Alcanzaste el limite de usuarios en tu plan.');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'phone' => 'required',
            'address' => 'required',
            'password' => 'required|max:128',
        ]);
        $subscription = Auth::user()->business->activeSubscription();
        if (Auth::user()->business->users->count() < $subscription->features()->code('entries.amount')->first()->limit) {
            $createduser = User::create([
              'first_name' => $request->first_name,
              'last_name' => $request->last_name,
              'email' => $request->email,
              'phone' => $request->phone,
              'address' > $request->address,
              'password' => bcrypt($request->password),
              'business_id' => Auth::user()->business->id,
            ]);
            return redirect(route('users.index'))->with('message', 'Creaste un nuevo usuario');
        }
        else {
            return redirect()->back()->with('warning', 'Alcanzaste el limite de usuarios en tu plan.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        return view('panel.users.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $validatedData = $request->validate([
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'phone' => 'required',
            'address' => 'required',
            'password' => 'required|max:128',
        ]);
        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->email = $request->email;
        $user->address = $request->address;
        $user->phone = $request->phone;
        if ($request->password != null) {
          $user->password = bcrypt($request->newpassword);
        }
        $user->save();

        return redirect(route('users.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        if ($user->id == Auth::user()->id) {
            return back()->with('warning', 'No puedes borrar tu cuenta');
        }
        if ($user->business->id != Auth::user()->business->id) {
            return back()->with('warning', 'No puedes borrar esta cuenta');
        }
        $user->delete();
        return back()->with('message', 'Se ha eliminado el usuario');
    }
}
