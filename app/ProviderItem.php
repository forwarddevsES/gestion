<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProviderItem extends Model
{
    protected $fillable = ['provider_id', 'name', 'code', 'price'];

    public function provider()
    {
      return $this->belongsTo('App\Provider');
    }
}
