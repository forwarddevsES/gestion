<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sale extends Model
{
    protected $fillable = ['business_id', 'client_id', 'final_price', 'status'];
    public function business()
    {
      return $this->belongsTo('App\Business');
    }
    public function client()
    {
      return $this->belongsTo('App\Client');
    }
    public function items()
    {
        return $this->hasMany('App\SaleItem');
    }
}
