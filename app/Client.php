<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    protected $fillable = ['business_id', 'name', 'email', 'address', 'phone', 'type']; //

    public function business()
    {
      return $this->belongsTo('App\Business');
    }
    public function sales()
    {
        return $this->hasMany('App\Sale');
    }
    public function entries()
    {
        return $this->hasMany('App\Entry');
    }
}
