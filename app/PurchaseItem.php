<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PurchaseItem extends Model
{
    protected $fillable = ['provider_item_id', 'purchase_id', 'amount'];

    public function purchase()
    {
      return $this->belongsTo('App\Purchase');
    }
    public function provider_item()
    {
      return $this->belongsTo('App\ProviderItem');
    }
}
