<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SaleItem extends Model
{
    protected $fillable = ['business_id', 'product_id', 'sale_id', 'amount', 'name', 'code', 'price'];
    public function business()
    {
      return $this->belongsTo('App\Business');
    }
    public function product()
    {
      return $this->belongsTo('App\Product');
    }
}
