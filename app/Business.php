<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Business extends Model
{


    protected $fillable = ['name', 'address', 'phone', 'email', 'city', 'cp', 'province'];


    public function products()
    {
        return $this->hasMany('App\Product');
    }
    public function providers()
    {
        return $this->hasMany('App\Provider');
    }
    public function clients()
    {
        return $this->hasMany('App\Client');
    }
    public function entries()
    {
        return $this->hasMany('App\Entry');
    }
    public function sales()
    {
        return $this->hasMany('App\Sale');
    }
    public function purchases()
    {
        return $this->hasMany('App\Purchase');
    }
    public function users()
    {
        return $this->hasMany('App\User');
    }
}
