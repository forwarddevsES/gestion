<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Provider extends Model
{
    protected $fillable = ['business_id', 'name', 'address', 'phone', 'email', 'info'];
    public function business()
    {
      return $this->belongsTo('App\Business');
    }
    public function items()
    {
      return $this->hasMany('App\ProviderItem');
    }
}
