<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Entry extends Model
{
    protected $fillable = ['business_id', 'client_id', 'type', 'notes', 'status', 'final_price', 'pagado', 'marca', 'modelo', 'serie', 'notas'];

    // @return App/User model.
    public function business()
    {
      return $this->belongsTo('App\Business');
    }
    public function client()
    {
      return $this->belongsTo('App\Client');
    }
}
