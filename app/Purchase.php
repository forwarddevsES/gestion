<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Purchase extends Model
{
    protected $fillable = ['business_id', 'provider_id', 'total_price'];
    public function provider()
    {
      return $this->belongsTo('App\Provider');
    }
    public function business()
    {
      return $this->belongsTo('App\Business');
    }
}
