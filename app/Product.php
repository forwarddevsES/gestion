<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = ['business_id', 'name', 'code', 'amount', 'price'];
    public function business()
    {
      return $this->belongsTo('App\Business');
    }
}
